<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Reserva;
use Faker\Generator as Faker;

$factory->define(Reserva::class, function (Faker $faker) {

    return [
        'fecha' => $faker->date('Y-m-d', $max = 'now'),
        'descripcion' => $faker->word,
        'user_id' => rand(1, 21),
        'estudiante_id' =>rand(1, 30),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'estado' => $faker->randomElement($array = array ('anulado','inscrito','reservado')),

    ];
});
