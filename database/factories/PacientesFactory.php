<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Paciente::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' => $faker->lastName,
        'genero' => $faker->randomElement($array = array ('m','f')),
        'fecha_nacimiento'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'telefono'=>$faker->phoneNumber,
    ];
});
