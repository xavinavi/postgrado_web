<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pago;
use Faker\Generator as Faker;

$factory->define(Pago::class, function (Faker $faker) {

    return [
        'fecha' => $faker->word,
        'monto' => $faker->randomDigitNotNull,
        'saldo' => $faker->randomDigitNotNull,
        'nombre' => $faker->text,
        'descripcion' => $faker->word,
        'inscripcion_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
