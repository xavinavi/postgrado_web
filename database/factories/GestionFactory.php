<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Gestion;
use Faker\Generator as Faker;

$factory->define(Gestion::class, function (Faker $faker) {

    return [
        'anio' => $faker->year($max = 'now'),
        'descripcion' => $faker->year($max = 'now').' años',
    ];
});
