<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\hola;
use Faker\Generator as Faker;

$factory->define(hola::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
