<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Periodo;
use Faker\Generator as Faker;

$factory->define(Periodo::class, function (Faker $faker) {

    return [
        'fecha_inicio' => $faker->date('Y-m-d', $max = 'now'),
        'fecha_fin' => $faker->date('Y-m-d', $max = 'now'),
        'descipcion' => $faker->word,
        'gestion_id' => $faker->randomDigitNotNull
    ];
});
