<?php

use Faker\Generator as Faker;

$factory->define(App\Models\AnalisisDetalle::class, function (Faker $faker) {
    return [
        'analisis_id' => rand(1, 120),
        'parametro_id' =>rand(1, 116),
    ];
});
