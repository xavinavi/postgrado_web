<?php

use Faker\Generator as Faker;
//$faker=Faker\Factory::create('es_VE');
//$faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
//App\Models\
$factory->define(App\Models\Empleado::class, function (Faker $faker) {

    return [
        'nombre' => $faker->name,
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' => $faker->lastName,
        'genero' => $faker->randomElement($array = array ('m','f')),
        'fecha_nacimiento'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'telefono'=>     $faker->phoneNumber,
        'direccion'=>$faker->address,
        'especialista_id'=>$faker->numberBetween($min=1,$max=4),

    ];
});
