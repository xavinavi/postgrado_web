<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Modulo;
use Faker\Generator as Faker;

$factory->define(Modulo::class, function (Faker $faker) {

    return [
        'nombre' => $faker->randomElement($array = array ('madures de procesos del software','gestion de calidad del software','tecnicas estrategias y herramientas de prueb d software '
        ,'tecnicas estrategias y herramientas de prueb software 	Aplicar tecnicas estrategias y herramientas para buenas pruebas en el software ','tecnicas y herramientas de hacking 	Aplicar tecnica estrategias y herramientas para buenas pruebas en el software'
        ,'la calidad y el proceso de desarrollo d software 	conceptos y aplicabilidad en el proceso del desarrollo de software'
        ,' en Administracion de Redes Convergentes '
            ,'Switching 	conceptos sobre comunicaciones lan redes de area local '
        ,'cableado estructurado y fibra optica')),
        'fecha_inicio' => $faker->date( 'Y-m-d', $max = 'now'), // $faker->year($max = 'now')
        'fecha_fin' => $faker->date( 'Y-m-d', $max = 'now'),
        'hora_inicio' => $faker->time( 'H:i:s', $max = 'now'),
        'hora_salida' => $faker->time( 'H:i:s', $max = 'now'),
        'costo' => $faker->randomDigitNotNull,
        'descripcion' => $faker->word,
        'programa_id' => $faker->numberBetween($min = 1, $max = 15),
        'profesor_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});


