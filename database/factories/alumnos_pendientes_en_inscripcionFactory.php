<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\alumnos_pendientes_en_inscripcion;
use Faker\Generator as Faker;

$factory->define(alumnos_pendientes_en_inscripcion::class, function (Faker $faker) {

    return [
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
