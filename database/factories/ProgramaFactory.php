<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Programa;
use Faker\Generator as Faker;

$factory->define(Programa::class, function (Faker $faker) {

    return [
        'nombre' => $faker->randomElement($array = array ('Maestria en Administracion de Redes Convergentes','Maestria en Ingenieria de Software','MAESTRIA EN CONECTIVIDAD Y REDES ALTERNATIVAS ')), // 'b',
        'duracion' => '2 años',
        'cupo_minimo' =>  $faker->numberBetween($min = 15, $max = 25),
        'descripcion' => $faker->word,
        'periodo_id' => $faker->randomDigitNotNull,
        'categoria_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
//Maestria en Administracion de Redes Convergentes
