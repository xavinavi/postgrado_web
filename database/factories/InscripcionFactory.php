<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Inscripcion;
use Faker\Generator as Faker;

$factory->define(Inscripcion::class, function (Faker $faker) {

    return [
        'fecha' => $faker->date('Y-m-d'),
        'user_id' => rand(1, 21),
        'estudiante_id' => rand(1, 30),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];



});
