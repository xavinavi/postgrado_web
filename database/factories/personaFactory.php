<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {


    return [
        'ci' => $faker->numberBetween($min = 10000000, $max = 90000000),
        'nombre' => $faker->name,
        'apellido_paterno' => $faker->lastName,
        'apellido_materno' => $faker->lastName,
        'telefono' => $faker->numberBetween($min = 60000000, $max = 79999999),
        'direccion' => $faker->address,
        'genero' =>  $faker->randomElement($array = array ('M','F')),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
