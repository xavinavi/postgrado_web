<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profesor;
use Faker\Generator as Faker;

$factory->define(Profesor::class, function (Faker $faker) {

    return [
      //  'cargo' => $faker->randomElement($array = array ('Secretaria','Administrador','Contador','Mensajero')),

        'profesion' =>  $faker->randomElement($array = array ('Ing Sistema','Ing Informatico','Doctor Seguridad Informatica','Ing Telecomunicaciones')),
        'persona_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
