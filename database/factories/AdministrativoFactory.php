<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administrativo;
use Faker\Generator as Faker;

$factory->define(Administrativo::class, function (Faker $faker) {

    return [

        'cargo' => $faker->randomElement($array = array ('Secretaria','Administrador','Contador','Mensajero')),
        'persona_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
