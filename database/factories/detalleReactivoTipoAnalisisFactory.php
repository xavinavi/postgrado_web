<?php

use Faker\Generator as Faker;

$factory->define(App\Models\DetalleReactivoAnalisis::class, function (Faker $faker) {
    return [
        'reactivo_id' => rand(1, 18),
        'tipoAnalisis_id' =>rand(1, 6),
    ];
});
