<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcion_modulo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('inscripcion_id')->unsigned();
            $table->integer('modulo_id')->unsigned();
            $table->integer('calificaciones')->unsigned()->nullable();

            // $table->softDeletes();
            $table->foreign('inscripcion_id')->references('id')->on('inscripcion');
            $table->foreign('modulo_id')->references('id')->on('modulos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcion_modulo');
    }
}
