<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModuloReserva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulo_reserva', function (Blueprint $table) {
            $table->integer('reserva_id')->unsigned();
            $table->integer('modulo_id')->unsigned();
            $table->timestamps();
           // $table->softDeletes();
            $table->foreign('reserva_id')->references('id')->on('reservas');
            $table->foreign('modulo_id')->references('id')->on('modulos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modulo_reserva');
    }
}
