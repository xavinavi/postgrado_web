<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombre');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->text('hora_inicio');
            $table->text('hora_salida');
            $table->float('costo');
            $table->longText('descripcion');
            $table->integer('programa_id')->unsigned();
            $table->integer('profesor_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('programa_id')->references('id')->on('programas')->onDelete('cascade');
            $table->foreign('profesor_id')->references('id')->on('profesores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //  Schema::drop('modulo_reserva');
      //  Schema::drop('inscripcion_modulo');
        Schema::drop('modulos');
    }
}
