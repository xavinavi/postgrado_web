<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('urls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });


        Schema::create('visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('url_id');
            $table->foreign('url_id')->references('id')->on('urls')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('cantidad')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
        Schema::dropIfExists('urls');
    }
}
