<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->float('monto');
            $table->float('saldo');
            $table->text('nombre');
            $table->longText('descripcion');
            $table->integer('inscripcion_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('inscripcion_id')->references('id')->on('inscripcion')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pago_modulo');
        Schema::drop('pagos');
    }
}
