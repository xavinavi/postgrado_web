<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgramasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('programas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombre');
            $table->text('duracion');
            $table->integer('cupo_minimo');
            $table->longText('descripcion');
            $table->integer('periodo_id')->unsigned();
            $table->integer('categoria_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('periodo_id')->references('id')->on('periodo');
            $table->foreign('categoria_id')->references('id')->on('categoria');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programas');
    }
}
