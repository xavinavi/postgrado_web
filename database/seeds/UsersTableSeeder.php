<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\Models\User;
//use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        User::create([
        	'name'		=> 'javier',
            'email' => 'javier@gmail.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
           'estilo'=>'',
            'persona_id'=>1,
            'remember_token' => str_random(10),
        ]);



        User::create([
        	'name'		=> 'miguel',
            'email' => 'miguel@gmail.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
           'estilo'=>'',
            'persona_id'=>2,
            'remember_token' => str_random(10),
        ]);


       factory(App\Models\User::class, 20)->create();

        Role::create([
        	'name'		=> 'Admin',
        	'slug'  	=> 'slug',
        	'special' 	=> 'all-access'
        ]);


        DB::table('role_user')->insert(
            ['role_id' => 1, 'user_id' => 1 ,'created_at'=> now(),'updated_at'=> now()]
        );
        DB::table('role_user')->insert(
            ['role_id' => 1, 'user_id' => 2 ,'created_at'=> now(),'updated_at'=> now()]
        );

    }
}
