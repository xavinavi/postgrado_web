<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       $this->call(PermissionsTableSeeder::class);
       factory(App\Models\Persona::class, 30)->create();
       $this->call(UsersTableSeeder::class);
      factory(App\Models\Estudiante::class, 30)->create();
      factory(App\Models\Administrativo::class, 30)->create();
      factory(App\Models\Profesor::class, 30)->create();
      factory(App\Models\Gestion::class, 30)->create();


        factory(App\Models\Categoria::class, 30)->create();
   //     factory(App\Models\Gestion::class, 30)->create();

        factory(App\Models\Periodo::class, 30)->create();

        factory(App\Models\Programa::class, 15)->create();

        factory(App\Models\Modulo::class, 100)->create();


        $this->call(ReservaSeeder::class);
        $this->call(InscripcionTablleSeeder::class);




      //  factory(App\php ::class, 20)->create();
      //  $this->call(VisitasTableSeeder::class);



    }
}
