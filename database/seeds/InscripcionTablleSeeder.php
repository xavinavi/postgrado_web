<?php

use Illuminate\Database\Seeder;

class InscripcionTablleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Inscripcion::class, 30)->create();

        for ( $i=0; $i<=100;$i++){
            DB::table('inscripcion_modulo')->insert(
                [
                    'inscripcion_id' => rand(2,30),
                    'modulo_id' =>rand(2,100) ,
                    'calificaciones'=>rand(0,100),
                    'created_at'=> now(),
                    'updated_at'=> now()
                ]
            );
        }
    }
}
