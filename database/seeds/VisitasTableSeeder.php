<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//       // dd($request->getPathInfo());   // "/administradorUsuario/users"

          DB::table('urls')->insert(['nombre' => '*/users']);
          DB::table('urls')->insert(['nombre' => '*/users/create']);
          DB::table('urls')->insert(['nombre' => '*/users/*/edit']);
          DB::table('urls')->insert(['nombre' => '*/users/*']);
		  
		  

		  
		    
		  DB::table('urls')->insert(['nombre' => '*/personas']);
          DB::table('urls')->insert(['nombre' => '*/personas/create']);
          DB::table('urls')->insert(['nombre' => '*/personas/*/edit']);
          DB::table('urls')->insert(['nombre' => '*/personas/*']);
		  
		  
		    
		  
		  	  		     
		  DB::table('urls')->insert(['nombre' => '*/agendas']);
          DB::table('urls')->insert(['nombre' => '*/agendas/create']);
          DB::table('urls')->insert(['nombre' => '*/agendas/*/edit']);
          DB::table('urls')->insert(['nombre' => '*/agendas/*']);
		  
		   DB::table('urls')->insert(['nombre' => '*/estilos']);
      
		  
		 DB::table('urls')->insert(['nombre' => '*/search']);
      
		  
		 
		  
    }
}
