<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});






























Route::resource('personas', 'PersonaAPIController');



Route::resource('gestions', 'GestionAPIController');

Route::resource('categorias', 'CategoriaAPIController');

Route::resource('periodos', 'PeriodoAPIController');



Route::resource('programas', 'ProgramaAPIController');

Route::resource('ofertas', 'OfertaAPIController');



Route::resource('profesors', 'ProfesorAPIController');

Route::resource('modulos', 'ModuloAPIController');

Route::resource('estudiantes', 'EstudianteAPIController');

Route::resource('reservas', 'ReservaAPIController');

Route::resource('inscripcions', 'InscripcionAPIController');

Route::resource('pagos', 'PagoAPIController');

Route::resource('administrativos', 'AdministrativoAPIController');



