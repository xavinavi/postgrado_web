<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::prefix('programas')->group(function () {
    Route::resource('periodos', 'PeriodoController');
    Route::resource('gestions', 'GestionController');
    Route::resource('programas', 'ProgramaController');
    Route::get('programas/{id}/modulo', 'ProgramaController@programaModulo')->name('programa_modulo');
    Route::resource('modulos', 'ModuloController');
    Route::resource('ofertas', 'OfertaController');
    Route::resource('categorias', 'CategoriaController');
});

Route::prefix('reservaInscripcion')->group(function () {
    Route::resource('reservas', 'ReservaController');
    Route::put('reserva/{id}/anular','ReservaController@anularReserva')->name('reserva.anular');
    Route::put('reserva/{id}/inscribir','ReservaController@inscribirReserva')->name('reserva.inscribir');

    Route::resource('inscripcions', 'InscripcionController');
    Route::get('inscripcions/{id}/materias', 'InscripcionController@materia')->name('inscripcion.materia'); // necesita el id inscripcion
    Route::post('inscripcions/{id}/materias/calificar', 'InscripcionController@calificar')->name('inscripcion.calificar'); // necesita el id inscripcion

    Route::get('pagar/Inscripcion/{id}', 'PagoController@realizarPagoInscripcion')->name('pagar.inscripcion'); // necesita el id inscripcion
    Route::put('pago/Inscripcion/{id}', 'PagoController@confirmarPagoInscripcion')->name('pagar.inscripcion.post');


    Route::resource('pagos', 'PagoController');

    Route::get('pagar/reserva/{id}', 'PagoController@realizarPagoReserva')->name('pagar.reserva'); // necesita el id de la reserva
    Route::put('pago/reserva/{id}', 'PagoController@confirmarPago')->name('pagar.reserva.post');
    //Route::get('pagar/Inscripcion/{id}', 'PagoController@realizarPagoInscripcion')->name('pagar.inscripcion'); // necesita el id de la reserva

    Route::get('pagar/SaladoInscripcion/{id}', 'PagoController@realizarPagoSaldoInscripcion')->name('pagar.saldo.inscripcion'); // necesita el id inscripcion
    Route::put('pagar/SaladoInscripcion/{id}', 'PagoController@confirmarPagoSaldoInscripcion')->name('pagar.saldo.post'); // necesita el id inscripcion



});


Route::prefix('administradorUsuario')->group(function () {
    Route::resource('users', 'UserController');
    Route::resource('profesors', 'ProfesorController');
    Route::resource('estudiantes', 'EstudianteController');
    Route::resource('administrativos', 'AdministrativoController');

    Route::resource('personas', 'PersonaController');


    Route::post('roles/store', 'RoleController@store')->name('roles.store')
        ->middleware('can:permission:roles.create');

    Route::get('roles', 'RoleController@index')->name('roles.index')
        ->middleware('can:permission:roles.index');

    Route::get('roles/create', 'RoleController@create')->name('roles.create')
        ->middleware('can:permission:roles.create');

    Route::PATCH('roles/{role}', 'RoleController@update')->name('roles.update')
        ->middleware('can:permission:roles.edit');

    Route::get('roles/{role}', 'RoleController@show')->name('roles.show')
        ->middleware('can:permission:roles.show');

    Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy')
        ->middleware('can:permission:roles.destroy');

    Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit')
        ->middleware('can:permission:roles.edit');

});

Route::prefix('reportes')->group(function () {
    Route::get('reporte_bi', 'ReporteController@reporte_bi')->name('reporte.reporte_bi');


    Route::get('alumnos_reprobados', 'ReporteController@alumnos_reprobados')->name('reporte.alumnos_reprobados');
    Route::get('PDF_alumnos_reprobados', 'ReporteController@PDF_alumnos_reprobados')->name('reporte.PDF_alumnos_reprobados');
    Route::get('estudiantes_deudores', 'ReporteController@estudiantes_deudores')->name('reporte.estudiantes_deudores');
    Route::get('PDF_estudiantes_deudores', 'ReporteController@PDF_estudiantes_deudores')->name('reporte.PDF_estudiantes_deudores');
    Route::get('lista_profesores_materia', 'ReporteController@lista_profesores_materia')->name('reporte.lista_profesores_materia');
    Route::get('PDF_lista_profesores_materia', 'ReporteController@PDF_lista_profesores_materia')->name('reporte.PDF_lista_profesores_materia');
    Route::get('recaudacion_por_modulo', 'ReporteController@recaudacion_por_modulo')->name('reporte.recaudacion_por_modulo');
    Route::get('PDF_recaudacion_por_modulo', 'ReporteController@PDF_recaudacion_por_modulo')->name('reporte.PDF_recaudacion_por_modulo');
    Route::get('cantidad_alumnos_modulo', 'ReporteController@cantidad_alumnos_modulo')->name('reporte.cantidad_alumnos_modulo');
    Route::get('PDF_cantidad_alumnos_modulo', 'ReporteController@PDF_cantidad_alumnos_modulo')->name('reporte.PDF_cantidad_alumnos_modulo');


    Route::get('alumnos_pendientes_en_inscripcion', 'ReporteController@alumnos_pendientes_en_inscripcion')->name('reporte.alumnos_pendientes_en_inscripcion');
    Route::get('PDF_alumnos_pendientes_en_inscripcion', 'ReporteController@PDF_alumnos_pendientes_en_inscripcion')->name('reporte.PDF_alumnos_pendientes_en_inscripcion');

});









Route::prefix('Reporte')->group(function () {
   /// Route::get('ordenPdf/{id}', 'OrdenController@exportarPdf')->name('reporte.ordenPdf');
});



Route::get('/home', 'HomeController@index')->name('home');


//Route::get('agenda', 'AgendaController@index');


/////  busqueda
Route::post('/search', 'HomeController@search')->name('search');

///// estilos

Route::get('/estilos', 'HomeController@estilosCreate')->name('estilos.index');
Route::get('/estilos/{id}/create', 'HomeController@estilosStoreTemplete')->name('estilos.store.templete');
Route::post('/estilos/font', 'HomeController@estilosStoregeFont')->name('estilos.store.font');
Route::get('/estilos/restablecer', 'HomeController@estilosRestablecer')->name('estilos.restablecer');

/// visitas

Route::post('/visitas', 'HomeController@vistaStore')->name('visita.store');



Route::resource('agendas', 'AgendaController');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');










































