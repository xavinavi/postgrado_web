<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Modulo",
 *      required={"nombre", "fecha_inicio", "fecha_fin", "hora_inicio", "hora_salida", "costo", "programa_id", "profesor_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fecha_inicio",
 *          description="fecha_inicio",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="fecha_fin",
 *          description="fecha_fin",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="hora_inicio",
 *          description="hora_inicio",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="hora_salida",
 *          description="hora_salida",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="costo",
 *          description="costo",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="programa_id",
 *          description="programa_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="profesor_id",
 *          description="profesor_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Modulo extends Model  implements  Searchable
{
    use SoftDeletes;

    public $table = 'modulos';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'fecha_inicio',
        'fecha_fin',
        'hora_inicio',
        'hora_salida',
        'costo',
        'descripcion',
        'programa_id',
        'profesor_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'fecha_inicio' => 'date',
        'fecha_fin' => 'date',
        'hora_inicio' => 'string',
        'hora_salida' => 'string',
        'costo' => 'float',
        'programa_id' => 'integer',
        'profesor_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'fecha_inicio' => 'required',
        'fecha_fin' => 'required',
        'hora_inicio' => 'required',
        'hora_salida' => 'required',
        'costo' => 'required|numeric|min:0',
        'programa_id' => 'required',
        'profesor_id' => 'required'
        //        'cupo_minimo' => 'required|numeric|digits_between:1,3',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Profesor()
    {
        return $this->belongsTo(\App\Models\Profesor::class, 'profesor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Programa()
    {
        return $this->belongsTo(\App\Models\Programa::class,  'programa_id');
    }

    public function reservas()
    {
        return $this->belongsToMany(\App\Models\Reserva::class, 'modulo_reserva', 'reserva_id', 'modulo_id');
    }

    public function Pagos()
    {
        return $this->belongsToMany(\App\Models\Pago::class, 'pago_modulo', 'pago_id', 'modulo_id')->withPivot('monto');
    }

    public function getSearchResult(): SearchResult
    {
        $url = route('modulos.show', $this->id);

        return new SearchResult($this, $this->nombre,
            $url
        );
    }
}
