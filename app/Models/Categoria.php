<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Categoria",
 *      required={"nombre"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Categoria extends Model  implements  Searchable
{
    use SoftDeletes;

    public $table = 'categoria';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'descripcion'
    ];


    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string'
    ];


    public static $rules = [
        'nombre' => 'required'
    ];

    public function Programa()
    {
        return $this->hasMany(\App\Models\Programa::class,  'categoria_id');
    }
    public function getSearchResult(): SearchResult
    {
        $url = route('categorias.show', $this->id);

        return new SearchResult($this, $this->nombre.' - '.$this->descripcion,
            $url
        );
    }

}
