<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Pago",
 *      required={"fecha", "monto", "saldo", "nombre", "inscripcion_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="fecha",
 *          description="fecha",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="monto",
 *          description="monto",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="saldo",
 *          description="saldo",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="inscripcion_id",
 *          description="inscripcion_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Pago extends Model
{
    use SoftDeletes;

    public $table = 'pagos';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'fecha',
        'monto',
        'saldo',
        'nombre',
        'descripcion',
        'inscripcion_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date',
        'monto' => 'float',
        'saldo' => 'float',
        'nombre' => 'string',
        'inscripcion_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha' => 'required',
        'monto' => 'required',
        'saldo' => 'required',
        'nombre' => 'required',
        'inscripcion_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Inscripcion()
    {
        return $this->belongsTo(\App\Models\Inscripcion::class,  'inscripcion_id');
    }

    public function modulos()
    {
        return $this->belongsToMany(\App\Models\Modulo::class, 'pago_modulo', 'pago_id', 'modulo_id')->withPivot('monto');
    }


    public function inscripciones()
    {
        return $this->belongsToMany(\App\Models\Inscripcion::class, 'inscripcion_pago', 'pago_id', 'inscripcion_id')->withPivot('pago','fecha');
    }
}
