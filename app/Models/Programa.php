<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Programa",
 *      required={"nombre", "duracion", "cupo_minimo", "descripcion"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="duracion",
 *          description="duracion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cupo_minimo",
 *          description="cupo_minimo",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="periodo_id",
 *          description="periodo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="categoria_id",
 *          description="categoria_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Programa extends Model implements  Searchable
{
    use SoftDeletes;

    public $table = 'programas';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'duracion',
        'cupo_minimo',
        'descripcion',
        'periodo_id',
        'categoria_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'duracion' => 'string',
        'cupo_minimo' => 'integer',
        'periodo_id' => 'integer',
        'categoria_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|min:6',
        'duracion' => 'required',
        'cupo_minimo' => 'required|numeric|digits_between:1,3',
      //  'descripcion' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Categoria()
    {
        return $this->belongsTo(\App\Models\Categoria::class,  'categoria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Periodo()
    {
        return $this->belongsTo(\App\Models\Periodo::class,  'periodo_id');
    }

    public function Modulo()
    {
        return $this->hasMany(\App\Models\Modulo::class,  'programa_id');
    }


    public function getSearchResult(): SearchResult
    {
        $url = route('programas.show', $this->id);

        return new SearchResult($this, $this->nombre.' - '.$this->duracion,
            $url
        );
    }
}
