<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Persona;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Administrativo",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cargo",
 *          description="cargo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="persona_id",
 *          description="persona_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Administrativo extends Model implements  Searchable
{
    use SoftDeletes;

    public $table = 'administrativos';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cargo',
        'persona_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cargo' => 'string',
        'persona_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Persona()
    {
        return $this->belongsTo(\App\Models\Persona::class,  'persona_id');
    }


    public function getSearchResult(): SearchResult
    {
        $url = route('administrativos.show', $this->id);
        $administrador = Persona::join('administrativos','personas.id','administrativos.persona_id')
            ->select('personas.nombre','personas.apellido_paterno','personas.apellido_materno','personas.direccion')->get();
           // dd($administrador);

        return new SearchResult(
            $this, $this->cargo,
            $url
        );
    }
}
