<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Profesor",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="profesion",
 *          description="profesion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="persona_id",
 *          description="persona_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Profesor extends Model
{
    use SoftDeletes;

    public $table = 'profesores';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'profesion',
        'persona_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'profesion' => 'string',
        'persona_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Persona()
    {
        return $this->belongsTo(\App\Models\Persona::class,  'persona_id');
    }

    public function getSearchResult(): SearchResult
    {
        $url = route('profesors.show', $this->id);

        return new SearchResult(
            $this,
            $this->name.' - '.$this->email,
            $url
        );
    }
}
