<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Gestion",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="año",
 *          description="año",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      )
 * )
 */
class Gestion extends Model implements  Searchable
{
    use SoftDeletes;

    public $table = 'gestion';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'anio',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'anio' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'anio'=> 'required',
        'descripcion'=> 'required',
    ];
    public function getSearchResult(): SearchResult
    {
        $url = route('gestions.show', $this->id);

        return new SearchResult($this,$this->descripcion,
            $url
        );
    }

}
