<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;


class Agenda extends Model implements Searchable
{
    use SoftDeletes;

    public $table = 'agendas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha_inicio',
        'fecha_fin',
        'titutlo',
        'descripcion',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha_inicio' => 'date',
        'fecha_fin' => 'date',
        'titutlo' => 'string',
        'descripcion' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        'fecha_inicio' => 'required',
        'fecha_fin' => 'required',
        'titutlo' => 'required',
        'descripcion' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }


    public function getSearchResult(): SearchResult
    {
        $url = route('agendas.show', $this->id);

        return new SearchResult(
            $this,
            $this->titutlo,
            $url
        );
    }
}
