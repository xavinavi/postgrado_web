<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
/**
 * @SWG\Definition(
 *      definition="Periodo",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="feha_inicio",
 *          description="feha_inicio",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="feha_fin",
 *          description="feha_fin",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="descipcion",
 *          description="descipcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gestion_id",
 *          description="gestion_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Periodo extends Model
{
    use SoftDeletes;

    public $table = 'periodo';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'fecha_inicio',
        'fecha_fin',
        'descipcion',
        'gestion_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha_inicio' => 'date',
        'fecha_fin' => 'date',
        'gestion_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha_inicio' => 'required',
        'fecha_fin' => 'required',
        'gestion_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function gestions()
    {
        return $this->belongsTo(\App\Models\Gestion::class,  'gestion_id');
    }
}
