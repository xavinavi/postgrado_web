<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property integer $user_id
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $titutlo
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Agenda extends Model
{
    use SoftDeletes;

    public $table = 'agendas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'fecha_inicio', 'fecha_fin', 'titutlo', 'descripcion'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
