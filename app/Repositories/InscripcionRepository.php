<?php

namespace App\Repositories;

use App\Models\Inscripcion;
use App\Repositories\BaseRepository;

/**
 * Class InscripcionRepository
 * @package App\Repositories
 * @version December 10, 2019, 4:56 pm UTC
*/

class InscripcionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'user_id',
        'estudiante_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inscripcion::class;
    }
}
