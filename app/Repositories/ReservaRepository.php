<?php

namespace App\Repositories;

use App\Models\Reserva;
use App\Repositories\BaseRepository;

/**
 * Class ReservaRepository
 * @package App\Repositories
 * @version December 10, 2019, 11:27 pm UTC
*/

class ReservaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'descripcion',
        'user_id',
        'estudiante_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reserva::class;
    }
}
