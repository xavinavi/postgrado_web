<?php

namespace App\Repositories;

use App\Models\Modulo;
use App\Repositories\BaseRepository;

/**
 * Class ModuloRepository
 * @package App\Repositories
 * @version December 10, 2019, 4:18 pm UTC
*/

class ModuloRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'fecha_inicio',
        'fecha_fin',
        'hora_inicio',
        'hora_salida',
        'costo',
        'descripcion',
        'programa_id',
        'profesor_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Modulo::class;
    }
}
