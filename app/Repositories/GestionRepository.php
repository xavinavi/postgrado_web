<?php

namespace App\Repositories;

use App\Models\Gestion;
use App\Repositories\BaseRepository;

/**
 * Class GestionRepository
 * @package App\Repositories
 * @version December 9, 2019, 10:21 pm UTC
*/

class GestionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'anio',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gestion::class;
    }
}
