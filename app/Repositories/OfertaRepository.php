<?php

namespace App\Repositories;

use App\Models\Oferta;
use App\Repositories\BaseRepository;

/**
 * Class OfertaRepository
 * @package App\Repositories
 * @version December 10, 2019, 3:34 pm UTC
*/

class OfertaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'fecha',
        'descripcion',
        'programa_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Oferta::class;
    }
}
