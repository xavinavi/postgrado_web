<?php

namespace App\Repositories;

use App\Models\Estudiante;
use App\Repositories\BaseRepository;

/**
 * Class EstudianteRepository
 * @package App\Repositories
 * @version December 10, 2019, 4:38 pm UTC
*/

class EstudianteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'registro',
        'persona_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estudiante::class;
    }
}
