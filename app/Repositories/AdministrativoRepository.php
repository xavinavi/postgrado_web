<?php

namespace App\Repositories;

use App\Models\Administrativo;
use App\Repositories\BaseRepository;

/**
 * Class AdministrativoRepository
 * @package App\Repositories
 * @version December 10, 2019, 6:37 pm UTC
*/

class AdministrativoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cargo',
        'persona_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Administrativo::class;
    }
}
