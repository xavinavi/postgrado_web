<?php

namespace App\Repositories;

use App\Models\Programa;
use App\Repositories\BaseRepository;

/**
 * Class ProgramaRepository
 * @package App\Repositories
 * @version December 10, 2019, 3:17 pm UTC
*/

class ProgramaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'duracion',
        'cupo_minimo',
        'descripcion',
        'periodo_id',
        'categoria_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Programa::class;
    }
}
