<?php

namespace App\Repositories;

use App\Models\Profesor;
use App\Repositories\BaseRepository;

/**
 * Class ProfesorRepository
 * @package App\Repositories
 * @version December 10, 2019, 4:04 pm UTC
*/

class ProfesorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'profesion',
        'persona_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Profesor::class;
    }
}
