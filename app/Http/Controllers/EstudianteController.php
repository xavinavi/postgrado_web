<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstudianteRequest;
use App\Http\Requests\UpdateEstudianteRequest;
use App\Repositories\EstudianteRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Estudiante;
use App\Models\Persona;
use Illuminate\Http\Request;
use Flash;
use Response;

class EstudianteController extends AppBaseController
{
    /** @var  EstudianteRepository */
    private $estudianteRepository;

    public function __construct(EstudianteRepository $estudianteRepo)
    {
        $this->estudianteRepository = $estudianteRepo;
    }

    /**
     * Display a listing of the Estudiante.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $estudiantes = Estudiante::join('personas', 'estudiantes.persona_id', 'personas.id')
        ->select('ci', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'direccion','genero' ,'registro' , 'estudiantes.id')
        ->paginate(10);

        return view('estudiantes.index')
            ->with('estudiantes', $estudiantes);
    }

    /**
     * Show the form for creating a new Estudiante.
     *
     * @return Response
     */
    public function create()
    {
        return view('estudiantes.create');
    }

    /**
     * Store a newly created Estudiante in storage.
     *
     * @param CreateEstudianteRequest $request
     *
     * @return Response
     */
    public function store(CreateEstudianteRequest $request)
    {
        $this->validate($request, [
            'ci' => 'required',
            'nombre' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'telefono' => 'required',
            'direccion' => 'required',
            'genero' => 'required'
        ]);
        $persona = new Persona();

        $persona->ci = $request->ci;
        $persona->nombre = $request->nombre;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->genero = $request->genero;

        $persona->save();

        $estudiante = new Estudiante();

        $estudiante->registro = date('Y').$persona->id;
        $estudiante->persona_id = $persona->id;

        $estudiante->save();

        Flash::success('Estudiante saved successfully.');

        return redirect(route('estudiantes.index'));
    }

    /**
     * Display the specified Estudiante.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estudiante = $this->estudianteRepository->find($id);

        if (empty($estudiante)) {
            Flash::error('Estudiante not found');

            return redirect(route('estudiantes.index'));
        }

        return view('estudiantes.show')->with('estudiante', $estudiante);
    }

    /**
     * Show the form for editing the specified Estudiante.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estudiante = Estudiante::join('personas', 'estudiantes.persona_id', 'personas.id')
        ->where('estudiantes.id' , $id)
        ->select('ci', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'direccion','genero', 'estudiantes.id')
        ->get();

        return view('estudiantes.edit')->with('estudiante', $estudiante[0]);
    }

    /**
     * Update the specified Estudiante in storage.
     *
     * @param int $id
     * @param UpdateEstudianteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstudianteRequest $request)
    {
        $this->validate($request, [
            'ci' => 'required',
            'nombre' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'telefono' => 'required',
            'direccion' => 'required',
            'genero' => 'required'
        ]);
        $estudiante = Estudiante::findOrFail($id);

        $persona = Persona::findOrFail($estudiante->persona_id);

        $persona->ci = $request->ci;
        $persona->nombre = $request->nombre;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->genero = $request->genero;

        $persona->update();

        Flash::success('Estudiante updated successfully.');

        return redirect(route('estudiantes.index'));
    }

    /**
     * Remove the specified Estudiante from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estudiante = $this->estudianteRepository->find($id);

        if (empty($estudiante)) {
            Flash::error('Estudiante not found');

            return redirect(route('estudiantes.index'));
        }

        $this->estudianteRepository->delete($id);

        Flash::success('Estudiante deleted successfully.');

        return redirect(route('estudiantes.index'));
    }
}
