<?php

namespace App\Http\Controllers;
use  Barryvdh\DomPDF\Facade as PDF;


use Illuminate\Http\Request;
use DB;
class ReporteController extends Controller
{
    //alumnos_pendientes_en_inscripcion


    public function alumnos_reprobados(Request $request)
    {
        $datos = DB::table('alumnos_reprobados')->paginate(10);

        return view('reportes.alumnos_reprobados', compact('datos'));
    }

    public function PDF_alumnos_reprobados(Request $request)
    {
        $datos = DB::table('alumnos_reprobados')->paginate(10);

        $datos = DB::table('alumnos_reprobados')->get();
        $pdf=PDF::loadView('reportes.PDF_alumnos_reprobados',compact('datos'));
        return $pdf->download('PDF_alumnos_reprobados.pdf');
    }


    public function cantidad_alumnos_modulo(Request $request)
    {
        $datos = DB::table('cantidad_alumnos_modulo')->paginate(10);

        return view('reportes.cantidad_alumnos_modulo', compact('datos'));
    }


    public function PDF_cantidad_alumnos_modulo(Request $request)
    {
        $datos = DB::table('cantidad_alumnos_modulo')->get();
        $pdf=PDF::loadView('reportes.PDF_cantidad_alumnos_modulo',compact('datos'));
        return $pdf->download('PDF_cantidad_alumnos_modulo.pdf');
    }



    public function estudiantes_deudores(Request $request)
    {
        $datos = DB::table('estudiantes_deudores')->paginate(10);

        return view('reportes.estudiantes_deudores', compact('datos'));
    }

    public function PDF_estudiantes_deudores(Request $request)
    {
        $datos = DB::table('estudiantes_deudores')->get();
        $pdf=PDF::loadView('reportes.PDF_estudiantes_deudores',compact('datos'));
        return $pdf->download('PDF_estudiantes_deudores.pdf');
    }


    public function lista_profesores_materia(Request $request)
    {
        $datos = DB::table('lista_profesores_materia')->paginate(10);

        return view('reportes.lista_profesores_materia', compact('datos'));
    }


    public function PDF_lista_profesores_materia(Request $request)
    {

        $datos = DB::table('lista_profesores_materia')->get();
        $pdf=PDF::loadView('reportes.PDF_lista_profesores_materia',compact('datos'));
        return $pdf->download('PDF_lista_profesores_materia.pdf');
    }


    public function recaudacion_por_modulo(Request $request)
    {
        $datos = DB::table('recaudacion_por_modulo')->paginate(10);

        return view('reportes.recaudacion_por_modulo', compact('datos'));
    }


    public function PDF_recaudacion_por_modulo(Request $request)
    {
        $datos = DB::table('recaudacion_por_modulo')->get();
        $pdf=PDF::loadView('reportes.PDF_recaudacion_por_modulo',compact('datos'));
        return $pdf->download('PDF_recaudacion_por_modulo.pdf');
    }






    public function reporte_bi()
    {
        return view('reportes.reporteBI');
    }


    public function alumnos_pendientes_en_inscripcion(Request $request)
    {
        $datos = DB::table('alumnos_pendientes_en_inscripcion')->paginate(10);

        return view('reportes.alumnos_pendientes_en_inscripcion', compact('datos'));
    }



    public function PDF_alumnos_pendientes_en_inscripcion(){
        $datos = DB::table('alumnos_pendientes_en_inscripcion')->get();
        $pdf=PDF::loadView('reportes.PDF_alumnos_pendientes_en_inscripcion',compact('datos'));
        return $pdf->download('PDF_alumnos_pendientes_en_inscripcion.pdf');
    }

}
