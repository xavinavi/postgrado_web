<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests\CreateInscripcionRequest;
use App\Http\Requests\UpdateInscripcionRequest;
use App\Models\Categoria;
use App\Models\Estudiante;
use App\Models\Inscripcion;
use App\Models\Modulo;
use App\Models\Reserva;
use App\Repositories\InscripcionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;
class InscripcionController extends AppBaseController
{
    /** @var  InscripcionRepository */
    private $inscripcionRepository;

    public function __construct(InscripcionRepository $inscripcionRepo)
    {
        $this->inscripcionRepository = $inscripcionRepo;
    }

    /**
     * Display a listing of the Inscripcion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $codigo=$request->get('codigo');
        $user=$request->get('user');
        $estudiante=$request->get('estudiante');
        $now = new \DateTime();

        $fecha = DB::table('inscripcion')
            ->orderBy('fecha', 'asc')
            ->first();
        $fecha=array($fecha->fecha ,$now->format('Y-m-d H:i:s'));
        if($request->get('fecha')) {

            $fecha = explode("to", $request->get('fecha'));
            $inscripciones =Inscripcion::orderBy('inscripcion.id', 'DESC')
                ->join('users', 'users.id', '=', 'inscripcion.user_id')
                ->join('estudiantes', 'estudiantes.id', '=', 'inscripcion.estudiante_id')
                ->join('personas', 'personas.id', '=', 'estudiantes.persona_id')
                ->where('personas.nombre','LIKE',"%$estudiante%")
                ->where('users.email','LIKE',"%$user%")
                ->where('inscripcion.id','LIKE',"%$codigo%")
                ->whereBetween('inscripcion.fecha',[$fecha[0],$fecha[1]])
                ->select('inscripcion.*')
                ->paginate(10)->setPath ( '' );

        }else{

            $inscripciones=Inscripcion::orderBy('inscripcion.id', 'DESC')
                ->join('users', 'users.id', '=', 'inscripcion.user_id')
                ->join('estudiantes', 'estudiantes.id', '=', 'inscripcion.estudiante_id')
                ->join('personas', 'personas.id', '=', 'estudiantes.persona_id')
                ->where('personas.nombre','LIKE',"%$estudiante%")
                //->orWhere('personas.apellido_paterno','LIKE',"%$estudiante%")
               // ->orWhere('personas.apellido_materno','LIKE',"%$estudiante%")
                ->where('users.email','LIKE',"%$user%")
                ->where('inscripcion.id','LIKE',"%$codigo%")
                ->select('inscripcion.*')
                ->paginate(10)->setPath ( '' );

        }

        $pagination = $inscripciones->appends ( array (
            'codigo' => $request->get( 'codigo' ),
            'estudiante' => $request->get( 'estudiante' ),
            'user' => $request->get( 'user' ),
            'fecha' => $request->get( 'fecha' )
        ) );

        return view('inscripcions.index',compact('inscripciones','codigo','user','estudiante','fecha'));
          //  ->with('inscripcions', $inscripciones);
    }





    public function materia($id)
    {
        $inscripcion=Inscripcion::find($id);


        return view('inscripcions.materias', compact('inscripcion'));
    }





    public function calificar(Request $request,$id)
    {
        $calificacion=$request->input('calificacion');;

        $v = Validator::make($request->all(), [
            'calificacion' => 'required|numeric|min:0|max:100',
        ]);
        if($v->fails())
        {
            Flash::warning('Ingrese un numero de un rango de 0 a 100.');

            return redirect()->back()->withErrors($v)->withInput();
        }

        DB::table('inscripcion_modulo')
            ->where('id', $id)
            ->update(['calificaciones' => $calificacion]);

        return  redirect()->back();
    }

    public function create()
    {
        $estudiantes=Estudiante::all();
        $categoria=Categoria::all();

        return view('inscripcions.create', compact('estudiantes','categoria'));
    }

    /**
     * Store a newly created Inscripcion in storage.
     *
     * @param CreateInscripcionRequest $request
     *
     * @return Response
     */

    public function store(CreateInscripcionRequest $request)
    {
        $input = $request->all();


        $inscripcion= new Inscripcion();
        $now = new \DateTime();
        $inscripcion->fecha=$now->format('Y-m-d H:i:s');
        $inscripcion->user_id=Auth::id();
        $inscripcion->estudiante_id=$request->input('estudiante_id');;
        $inscripcion->save();

        $inscripcion->modulos()->sync($request->get('modulos_id'));

      //  $inscripcion = $this->inscripcionRepository->create($input);


        Flash::success('Inscripcion saved successfully.');

        return redirect(route('pagar.inscripcion', $inscripcion->id));//create.pagos


      //  return redirect(route('inscripcions.index'));
    }

    /**
     * Display the specified Inscripcion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inscripcion = $this->inscripcionRepository->find($id);

        if (empty($inscripcion)) {
            Flash::error('Inscripcion not found');

            return redirect(route('inscripcions.index'));
        }

        return view('inscripcions.show')->with('inscripcion', $inscripcion);
    }

    /**
     * Show the form for editing the specified Inscripcion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inscripcion = $this->inscripcionRepository->find($id);

        if (empty($inscripcion)) {
            Flash::error('Inscripcion not found');

            return redirect(route('inscripcions.index'));
        }

        return view('inscripcions.edit')->with('inscripcion', $inscripcion);
    }

    /**
     * Update the specified Inscripcion in storage.
     *
     * @param int $id
     * @param UpdateInscripcionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInscripcionRequest $request)
    {
        $inscripcion = $this->inscripcionRepository->find($id);

        if (empty($inscripcion)) {
            Flash::error('Inscripcion not found');

            return redirect(route('inscripcions.index'));
        }

        $inscripcion = $this->inscripcionRepository->update($request->all(), $id);

        Flash::success('Inscripcion updated successfully.');

        return redirect(route('inscripcions.index'));
    }

    /**
     * Remove the specified Inscripcion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inscripcion = $this->inscripcionRepository->find($id);

        if (empty($inscripcion)) {
            Flash::error('Inscripcion not found');

            return redirect(route('inscripcions.index'));
        }

        $this->inscripcionRepository->delete($id);

        Flash::success('Inscripcion deleted successfully.');

        return redirect(route('inscripcions.index'));
    }
}
