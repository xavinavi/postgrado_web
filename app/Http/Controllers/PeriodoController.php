<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePeriodoRequest;
use App\Http\Requests\UpdatePeriodoRequest;
use App\Repositories\PeriodoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Gestion;

class PeriodoController extends AppBaseController
{
    /** @var  PeriodoRepository */
    private $periodoRepository;
    
    public function __construct(PeriodoRepository $periodoRepo)
    {
        $this->periodoRepository = $periodoRepo;
    }

    /**
     * Display a listing of the Periodo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $periodos = $this->periodoRepository->paginate(10);

        return view('periodos.index')
            ->with('periodos', $periodos);
    }

    /**
     * Show the form for creating a new Periodo.
     *
     * @return Response
     */
    public function create()
    {
        $gestion = Gestion::all();
        return view('periodos.create',compact('gestion'));
    }

    /**
     * Store a newly created Periodo in storage.
     *
     * @param CreatePeriodoRequest $request
     *
     * @return Response
     */
    public function store(CreatePeriodoRequest $request)
    {
        $input = $request->all();

        $periodo = $this->periodoRepository->create($input);

        Flash::success('Periodo saved successfully.');

        return redirect(route('periodos.index'));
    }

    /**
     * Display the specified Periodo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $periodo = $this->periodoRepository->find($id);
        $gestion = Gestion::all();

        if (empty($periodo)) {
            Flash::error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        return view('periodos.show', compact('gestion','periodo'));
    }

    /**
     * Show the form for editing the specified Periodo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $periodo = $this->periodoRepository->find($id);

        $gestion = Gestion::all();
        //return view('periodos.create',compact('gestion'));

        if (empty($periodo)) {
            Flash::error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        return view('periodos.edit',compact('gestion','periodo'));
    }

    /**
     * Update the specified Periodo in storage.
     *
     * @param int $id
     * @param UpdatePeriodoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePeriodoRequest $request)
    {
        $periodo = $this->periodoRepository->find($id);

        if (empty($periodo)) {
            Flash::error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        $periodo = $this->periodoRepository->update($request->all(), $id);

        Flash::success('Periodo updated successfully.');

        return redirect(route('periodos.index'));
    }

    /**
     * Remove the specified Periodo from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $periodo = $this->periodoRepository->find($id);

        if (empty($periodo)) {
            Flash::error('Periodo not found');

            return redirect(route('periodos.index'));
        }

        $this->periodoRepository->delete($id);

        Flash::success('Periodo deleted successfully.');

        return redirect(route('periodos.index'));
    }
}
