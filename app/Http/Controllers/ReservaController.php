<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReservaRequest;
use App\Http\Requests\UpdateReservaRequest;
use App\Models\Categoria;
use App\Models\Inscripcion;
use App\Models\Programa;
use App\Repositories\ReservaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Estudiante;
use App\Models\Modulo;
use App\Models\Reserva;
use Illuminate\Support\Facades\Auth;
use DB;
class ReservaController extends AppBaseController
{
    /** @var  ReservaRepository */
    private $reservaRepository;

    public function __construct(ReservaRepository $reservaRepo)
    {
        $this->reservaRepository = $reservaRepo;
    }

    /**
     * Display a listing of the Reserva.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $codigo=$request->get('codigo');
        $estado=$request->get('estado');
        $estudiante=$request->get('estudiante');
        $now = new \DateTime();

        $fecha = DB::table('reservas')
            ->orderBy('fecha', 'asc')
            ->first();
        $fecha=array($fecha->fecha ,$now->format('Y-m-d H:i:s'));
        if($request->get('fecha'))
        {
            $fecha = explode("to", $request->get('fecha'));
            $reservas=Reserva::orderBy('reservas.id', 'DESC')
                ->join('users', 'users.id', '=', 'reservas.user_id')
                ->join('estudiantes', 'estudiantes.id', '=', 'reservas.estudiante_id')
                ->join('personas', 'personas.id', '=', 'estudiantes.persona_id')
                ->where('personas.nombre','LIKE',"%$estudiante%")
               // ->orWhere('personas.apellido_paterno','LIKE',"%$estudiante%")
                //->orWhere('personas.apellido_materno','LIKE',"%$estudiante%")
                ->where('reservas.estado','LIKE',"%$estado%")
                ->where('reservas.id','LIKE',"%$codigo%")
                ->whereBetween('reservas.fecha',[$fecha[0],$fecha[1]])
                ->select('reservas.*')
                ->paginate(10)->setPath ( '' );


        }else{
            $reservas=Reserva::orderBy('reservas.id', 'DESC')
                ->join('users', 'users.id', '=', 'reservas.user_id')
                ->join('estudiantes', 'estudiantes.id', '=', 'reservas.estudiante_id')
                ->join('personas', 'personas.id', '=', 'estudiantes.persona_id')
                ->where('personas.nombre','LIKE',"%$estudiante%")
                //->orWhere('personas.apellido_paterno','LIKE',"%$estudiante%")
             //   ->orWhere('personas.apellido_materno','LIKE',"%$estudiante%")
                ->where('reservas.estado','LIKE',"%$estado%")
                ->where('reservas.id','LIKE',"%$codigo%")
                ->select('reservas.*')
                ->paginate(10)->setPath ( '' );

        }

        $pagination = $reservas->appends ( array (
            'codigo' => $request->get( 'codigo' ),
            'estado' => $request->get( 'estado' ),
            'estudiante' => $request->get( 'estudiante' ),
            'fecha' => $request->get( 'fecha' )
        ) );
        return view('reservas.index', compact('reservas','codigo','estado','estudiante','fecha'));


    }

    /**
     * Show the form for creating a new Reserva.
     *
     * @return Response
     */
    public function create(Request $request)
    {
       // dd($request->input('d'));
        $estudiantes=Estudiante::all();
        $categoria=Categoria::all();
        return view('reservas.create', compact('estudiantes','categoria'));
    }


    public function store(CreateReservaRequest $request)
    {
        $resera= new Reserva();
        $now = new \DateTime();
        $resera->fecha=$now->format('Y-m-d H:i:s');
        $resera->descripcion=$request->input('descripcion');
        $resera->user_id=Auth::id();
        $resera->estudiante_id=$request->input('estudiante_id');;
        $resera->estado='reservado';
        $resera->save();

        $resera->modulos()->sync($request->get('modulos_id'));
      //  $reserva = $this->reservaRepository->create($input);
///        $orden->analisis()->sync($request->get('analisis_id'));
        Flash::success('Reserva saved successfully.');

        return redirect(route('reservas.index'));
    }

    public function anularReserva($id)
    {
        $resera= Reserva::find($id);
        $resera->estado='anulado';
        $resera->save();
        return redirect()->back();
    }

  public function inscribirReserva($id)
    {
        $resera= Reserva::find($id);

        $resera->estado='inscrito';
        $resera->save();

        $modulos_id = DB::table('modulo_reserva')->where('reserva_id', $resera->id)->pluck('modulo_id');

        $inscripcion= new Inscripcion();
        $now = new \DateTime();
        $inscripcion->fecha=$now->format('Y-m-d H:i:s');
        $inscripcion->user_id=Auth::id();
        $inscripcion->estudiante_id=$resera->estudiante_id;
        $inscripcion->save();
        $inscripcion->modulos()->sync($modulos_id);

        return redirect(route('pagar.reserva', $inscripcion->id));//create.pagos
    }


    /**
     * Display the specified Reserva.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reserva = $this->reservaRepository->find($id);
        if (empty($reserva)) {
            Flash::error('Reserva not found');

            return redirect(route('reservas.index'));
        }

        return view('reservas.show',compact('reserva'));
    }

    /**
     * Show the form for editing the specified Reserva.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reserva = $this->reservaRepository->find($id);
        $estudiantes=Estudiante::all();
        $modulos=Modulo::all();
        if (empty($reserva)) {
            Flash::error('Reserva not found');

            return redirect(route('reservas.index'));
        }

        return view('reservas.edit',compact('reserva','estudiantes','modulos'));
    }

    /**
     * Update the specified Reserva in storage.
     *
     * @param int $id
     * @param UpdateReservaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReservaRequest $request)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva not found');

            return redirect(route('reservas.index'));
        }

        $reserva = $this->reservaRepository->update($request->all(), $id);

        Flash::success('Reserva updated successfully.');

        return redirect(route('reservas.index'));
    }

    /**
     * Remove the specified Reserva from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva not found');

            return redirect(route('reservas.index'));
        }

        $this->reservaRepository->delete($id);

        Flash::success('Reserva deleted successfully.');

        return redirect(route('reservas.index'));
    }
}
