<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdministrativoRequest;
use App\Http\Requests\UpdateAdministrativoRequest;
use App\Repositories\AdministrativoRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Administrativo;
use App\Models\Persona;
use Illuminate\Http\Request;
use Flash;
use Response;

class AdministrativoController extends AppBaseController
{
    /** @var  AdministrativoRepository */
    private $administrativoRepository;

    public function __construct(AdministrativoRepository $administrativoRepo)
    {
        $this->administrativoRepository = $administrativoRepo;
    }

    /**
     * Display a listing of the Administrativo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $administrativos = Administrativo::join('personas', 'administrativos.persona_id', 'personas.id')
        ->select('ci', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'direccion','genero' ,'cargo' , 'administrativos.id')
        ->paginate(10);

        return view('administrativos.index')
            ->with('administrativos', $administrativos);
    }

    /**
     * Show the form for creating a new Administrativo.
     *
     * @return Response
     */
    public function create()
    {
        return view('administrativos.create');
    }

    /**
     * Store a newly created Administrativo in storage.
     *
     * @param CreateAdministrativoRequest $request
     *
     * @return Response
     */
    public function store(CreateAdministrativoRequest $request)
    {
        $this->validate($request, [
            'ci' => 'required',
            'nombre' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'telefono' => 'required',
            'direccion' => 'required',
            'cargo' => 'required',
            'genero' => 'required'
        ]);
        $persona = new Persona();

        $persona->ci = $request->ci;
        $persona->nombre = $request->nombre;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->genero = $request->genero;

        $persona->save();

        $administrativo = new Administrativo();

        $administrativo->cargo = $request->cargo;
        $administrativo->persona_id = $persona->id;

        $administrativo->save();

        Flash::success('Administrativo saved successfully.');

        return redirect(route('administrativos.index'));
    }

    /**
     * Display the specified Administrativo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            Flash::error('Administrativo not found');

            return redirect(route('administrativos.index'));
        }

        return view('administrativos.show')->with('administrativo', $administrativo);
    }

    /**
     * Show the form for editing the specified Administrativo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $administrativo = Administrativo::join('personas', 'administrativos.persona_id', 'personas.id')
        ->where('administrativos.id', $id)
        ->select('ci', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'direccion','genero' ,'cargo' , 'administrativos.id')
        ->get();

        return view('administrativos.edit')->with('administrativo', $administrativo[0]);
    }

    /**
     * Update the specified Administrativo in storage.
     *
     * @param int $id
     * @param UpdateAdministrativoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdministrativoRequest $request)
    {
        $this->validate($request, [
            'ci' => 'required',
            'nombre' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'telefono' => 'required',
            'direccion' => 'required',
            'cargo' => 'required',
            'genero' => 'required'
        ]);
        $administrativo = Administrativo::findOrFail($id);

        $administrativo->cargo = $request->cargo;

        $administrativo->update();

        $persona = Persona::findOrFail($administrativo->persona_id);

        $persona->ci = $request->ci;
        $persona->nombre = $request->nombre;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->genero = $request->genero;

        $persona->update();

        Flash::success('Administrativo updated successfully.');

        return redirect(route('administrativos.index'));
    }

    /**
     * Remove the specified Administrativo from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            Flash::error('Administrativo not found');

            return redirect(route('administrativos.index'));
        }

        $this->administrativoRepository->delete($id);

        Flash::success('Administrativo deleted successfully.');

        return redirect(route('administrativos.index'));
    }
}
