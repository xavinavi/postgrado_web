<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePeriodoAPIRequest;
use App\Http\Requests\API\UpdatePeriodoAPIRequest;
use App\Models\Periodo;
use App\Repositories\PeriodoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PeriodoController
 * @package App\Http\Controllers\API
 */

class PeriodoAPIController extends AppBaseController
{
    /** @var  PeriodoRepository */
    private $periodoRepository;

    public function __construct(PeriodoRepository $periodoRepo)
    {
        $this->periodoRepository = $periodoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/periodos",
     *      summary="Get a listing of the Periodos.",
     *      tags={"Periodo"},
     *      description="Get all Periodos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Periodo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $periodos = $this->periodoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($periodos->toArray(), 'Periodos retrieved successfully');
    }

    /**
     * @param CreatePeriodoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/periodos",
     *      summary="Store a newly created Periodo in storage",
     *      tags={"Periodo"},
     *      description="Store Periodo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Periodo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Periodo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Periodo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePeriodoAPIRequest $request)
    {
        $input = $request->all();

        $periodo = $this->periodoRepository->create($input);

        return $this->sendResponse($periodo->toArray(), 'Periodo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/periodos/{id}",
     *      summary="Display the specified Periodo",
     *      tags={"Periodo"},
     *      description="Get Periodo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Periodo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Periodo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Periodo $periodo */
        $periodo = $this->periodoRepository->find($id);

        if (empty($periodo)) {
            return $this->sendError('Periodo not found');
        }

        return $this->sendResponse($periodo->toArray(), 'Periodo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePeriodoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/periodos/{id}",
     *      summary="Update the specified Periodo in storage",
     *      tags={"Periodo"},
     *      description="Update Periodo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Periodo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Periodo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Periodo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Periodo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePeriodoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Periodo $periodo */
        $periodo = $this->periodoRepository->find($id);

        if (empty($periodo)) {
            return $this->sendError('Periodo not found');
        }

        $periodo = $this->periodoRepository->update($input, $id);

        return $this->sendResponse($periodo->toArray(), 'Periodo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/periodos/{id}",
     *      summary="Remove the specified Periodo from storage",
     *      tags={"Periodo"},
     *      description="Delete Periodo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Periodo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Periodo $periodo */
        $periodo = $this->periodoRepository->find($id);

        if (empty($periodo)) {
            return $this->sendError('Periodo not found');
        }

        $periodo->delete();

        return $this->sendSuccess('Periodo deleted successfully');
    }
}
