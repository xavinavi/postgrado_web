<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProfesorAPIRequest;
use App\Http\Requests\API\UpdateProfesorAPIRequest;
use App\Models\Profesor;
use App\Repositories\ProfesorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProfesorController
 * @package App\Http\Controllers\API
 */

class ProfesorAPIController extends AppBaseController
{
    /** @var  ProfesorRepository */
    private $profesorRepository;

    public function __construct(ProfesorRepository $profesorRepo)
    {
        $this->profesorRepository = $profesorRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/profesors",
     *      summary="Get a listing of the Profesors.",
     *      tags={"Profesor"},
     *      description="Get all Profesors",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Profesor")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $profesors = $this->profesorRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($profesors->toArray(), 'Profesors retrieved successfully');
    }

    /**
     * @param CreateProfesorAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/profesors",
     *      summary="Store a newly created Profesor in storage",
     *      tags={"Profesor"},
     *      description="Store Profesor",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Profesor that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Profesor")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Profesor"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProfesorAPIRequest $request)
    {
        $input = $request->all();

        $profesor = $this->profesorRepository->create($input);

        return $this->sendResponse($profesor->toArray(), 'Profesor saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/profesors/{id}",
     *      summary="Display the specified Profesor",
     *      tags={"Profesor"},
     *      description="Get Profesor",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Profesor",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Profesor"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Profesor $profesor */
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            return $this->sendError('Profesor not found');
        }

        return $this->sendResponse($profesor->toArray(), 'Profesor retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProfesorAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/profesors/{id}",
     *      summary="Update the specified Profesor in storage",
     *      tags={"Profesor"},
     *      description="Update Profesor",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Profesor",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Profesor that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Profesor")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Profesor"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProfesorAPIRequest $request)
    {
        $input = $request->all();

        /** @var Profesor $profesor */
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            return $this->sendError('Profesor not found');
        }

        $profesor = $this->profesorRepository->update($input, $id);

        return $this->sendResponse($profesor->toArray(), 'Profesor updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/profesors/{id}",
     *      summary="Remove the specified Profesor from storage",
     *      tags={"Profesor"},
     *      description="Delete Profesor",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Profesor",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Profesor $profesor */
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            return $this->sendError('Profesor not found');
        }

        $profesor->delete();

        return $this->sendSuccess('Profesor deleted successfully');
    }
}
