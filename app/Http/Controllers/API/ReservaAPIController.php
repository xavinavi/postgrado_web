<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReservaAPIRequest;
use App\Http\Requests\API\UpdateReservaAPIRequest;
use App\Models\Reserva;
use App\Repositories\ReservaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ReservaController
 * @package App\Http\Controllers\API
 */

class ReservaAPIController extends AppBaseController
{
    /** @var  ReservaRepository */
    private $reservaRepository;

    public function __construct(ReservaRepository $reservaRepo)
    {
        $this->reservaRepository = $reservaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reservas",
     *      summary="Get a listing of the Reservas.",
     *      tags={"Reserva"},
     *      description="Get all Reservas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reserva")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $reservas = $this->reservaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($reservas->toArray(), 'Reservas retrieved successfully');
    }

    /**
     * @param CreateReservaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/reservas",
     *      summary="Store a newly created Reserva in storage",
     *      tags={"Reserva"},
     *      description="Store Reserva",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Reserva that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Reserva")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reserva"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateReservaAPIRequest $request)
    {
        $input = $request->all();

        $reserva = $this->reservaRepository->create($input);

        return $this->sendResponse($reserva->toArray(), 'Reserva saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/reservas/{id}",
     *      summary="Display the specified Reserva",
     *      tags={"Reserva"},
     *      description="Get Reserva",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reserva",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reserva"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Reserva $reserva */
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            return $this->sendError('Reserva not found');
        }

        return $this->sendResponse($reserva->toArray(), 'Reserva retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateReservaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/reservas/{id}",
     *      summary="Update the specified Reserva in storage",
     *      tags={"Reserva"},
     *      description="Update Reserva",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reserva",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Reserva that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Reserva")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Reserva"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateReservaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Reserva $reserva */
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            return $this->sendError('Reserva not found');
        }

        $reserva = $this->reservaRepository->update($input, $id);

        return $this->sendResponse($reserva->toArray(), 'Reserva updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/reservas/{id}",
     *      summary="Remove the specified Reserva from storage",
     *      tags={"Reserva"},
     *      description="Delete Reserva",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Reserva",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Reserva $reserva */
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            return $this->sendError('Reserva not found');
        }

        $reserva->delete();

        return $this->sendSuccess('Reserva deleted successfully');
    }
}
