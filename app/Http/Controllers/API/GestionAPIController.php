<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGestionAPIRequest;
use App\Http\Requests\API\UpdateGestionAPIRequest;
use App\Models\Gestion;
use App\Repositories\GestionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class GestionController
 * @package App\Http\Controllers\API
 */

class GestionAPIController extends AppBaseController
{
    /** @var  GestionRepository */
    private $gestionRepository;

    public function __construct(GestionRepository $gestionRepo)
    {
        $this->gestionRepository = $gestionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/gestions",
     *      summary="Get a listing of the Gestions.",
     *      tags={"Gestion"},
     *      description="Get all Gestions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Gestion")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $gestions = $this->gestionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($gestions->toArray(), 'Gestions retrieved successfully');
    }

    /**
     * @param CreateGestionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/gestions",
     *      summary="Store a newly created Gestion in storage",
     *      tags={"Gestion"},
     *      description="Store Gestion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Gestion that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Gestion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Gestion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateGestionAPIRequest $request)
    {
        $input = $request->all();

        $gestion = $this->gestionRepository->create($input);

        return $this->sendResponse($gestion->toArray(), 'Gestion saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/gestions/{id}",
     *      summary="Display the specified Gestion",
     *      tags={"Gestion"},
     *      description="Get Gestion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Gestion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Gestion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Gestion $gestion */
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            return $this->sendError('Gestion not found');
        }

        return $this->sendResponse($gestion->toArray(), 'Gestion retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateGestionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/gestions/{id}",
     *      summary="Update the specified Gestion in storage",
     *      tags={"Gestion"},
     *      description="Update Gestion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Gestion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Gestion that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Gestion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Gestion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateGestionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Gestion $gestion */
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            return $this->sendError('Gestion not found');
        }

        $gestion = $this->gestionRepository->update($input, $id);

        return $this->sendResponse($gestion->toArray(), 'Gestion updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/gestions/{id}",
     *      summary="Remove the specified Gestion from storage",
     *      tags={"Gestion"},
     *      description="Delete Gestion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Gestion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Gestion $gestion */
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            return $this->sendError('Gestion not found');
        }

        $gestion->delete();

        return $this->sendSuccess('Gestion deleted successfully');
    }
}
