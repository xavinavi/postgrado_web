<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProgramaAPIRequest;
use App\Http\Requests\API\UpdateProgramaAPIRequest;
use App\Models\Programa;
use App\Repositories\ProgramaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProgramaController
 * @package App\Http\Controllers\API
 */

class ProgramaAPIController extends AppBaseController
{
    /** @var  ProgramaRepository */
    private $programaRepository;

    public function __construct(ProgramaRepository $programaRepo)
    {
        $this->programaRepository = $programaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/programas",
     *      summary="Get a listing of the Programas.",
     *      tags={"Programa"},
     *      description="Get all Programas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Programa")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $programas = $this->programaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($programas->toArray(), 'Programas retrieved successfully');
    }

    /**
     * @param CreateProgramaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/programas",
     *      summary="Store a newly created Programa in storage",
     *      tags={"Programa"},
     *      description="Store Programa",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Programa that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Programa")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Programa"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProgramaAPIRequest $request)
    {
        $input = $request->all();

        $programa = $this->programaRepository->create($input);

        return $this->sendResponse($programa->toArray(), 'Programa saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/programas/{id}",
     *      summary="Display the specified Programa",
     *      tags={"Programa"},
     *      description="Get Programa",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Programa",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Programa"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Programa $programa */
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            return $this->sendError('Programa not found');
        }

        return $this->sendResponse($programa->toArray(), 'Programa retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProgramaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/programas/{id}",
     *      summary="Update the specified Programa in storage",
     *      tags={"Programa"},
     *      description="Update Programa",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Programa",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Programa that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Programa")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Programa"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProgramaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Programa $programa */
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            return $this->sendError('Programa not found');
        }

        $programa = $this->programaRepository->update($input, $id);

        return $this->sendResponse($programa->toArray(), 'Programa updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/programas/{id}",
     *      summary="Remove the specified Programa from storage",
     *      tags={"Programa"},
     *      description="Delete Programa",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Programa",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Programa $programa */
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            return $this->sendError('Programa not found');
        }

        $programa->delete();

        return $this->sendSuccess('Programa deleted successfully');
    }
}
