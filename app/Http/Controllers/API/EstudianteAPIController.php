<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEstudianteAPIRequest;
use App\Http\Requests\API\UpdateEstudianteAPIRequest;
use App\Models\Estudiante;
use App\Repositories\EstudianteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EstudianteController
 * @package App\Http\Controllers\API
 */

class EstudianteAPIController extends AppBaseController
{
    /** @var  EstudianteRepository */
    private $estudianteRepository;

    public function __construct(EstudianteRepository $estudianteRepo)
    {
        $this->estudianteRepository = $estudianteRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/estudiantes",
     *      summary="Get a listing of the Estudiantes.",
     *      tags={"Estudiante"},
     *      description="Get all Estudiantes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Estudiante")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $estudiantes = $this->estudianteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($estudiantes->toArray(), 'Estudiantes retrieved successfully');
    }

    /**
     * @param CreateEstudianteAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/estudiantes",
     *      summary="Store a newly created Estudiante in storage",
     *      tags={"Estudiante"},
     *      description="Store Estudiante",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Estudiante that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Estudiante")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Estudiante"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEstudianteAPIRequest $request)
    {
        $input = $request->all();

        $estudiante = $this->estudianteRepository->create($input);

        return $this->sendResponse($estudiante->toArray(), 'Estudiante saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/estudiantes/{id}",
     *      summary="Display the specified Estudiante",
     *      tags={"Estudiante"},
     *      description="Get Estudiante",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Estudiante",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Estudiante"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Estudiante $estudiante */
        $estudiante = $this->estudianteRepository->find($id);

        if (empty($estudiante)) {
            return $this->sendError('Estudiante not found');
        }

        return $this->sendResponse($estudiante->toArray(), 'Estudiante retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEstudianteAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/estudiantes/{id}",
     *      summary="Update the specified Estudiante in storage",
     *      tags={"Estudiante"},
     *      description="Update Estudiante",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Estudiante",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Estudiante that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Estudiante")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Estudiante"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEstudianteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Estudiante $estudiante */
        $estudiante = $this->estudianteRepository->find($id);

        if (empty($estudiante)) {
            return $this->sendError('Estudiante not found');
        }

        $estudiante = $this->estudianteRepository->update($input, $id);

        return $this->sendResponse($estudiante->toArray(), 'Estudiante updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/estudiantes/{id}",
     *      summary="Remove the specified Estudiante from storage",
     *      tags={"Estudiante"},
     *      description="Delete Estudiante",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Estudiante",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Estudiante $estudiante */
        $estudiante = $this->estudianteRepository->find($id);

        if (empty($estudiante)) {
            return $this->sendError('Estudiante not found');
        }

        $estudiante->delete();

        return $this->sendSuccess('Estudiante deleted successfully');
    }
}
