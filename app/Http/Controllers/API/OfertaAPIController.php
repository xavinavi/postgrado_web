<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOfertaAPIRequest;
use App\Http\Requests\API\UpdateOfertaAPIRequest;
use App\Models\Oferta;
use App\Repositories\OfertaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OfertaController
 * @package App\Http\Controllers\API
 */

class OfertaAPIController extends AppBaseController
{
    /** @var  OfertaRepository */
    private $ofertaRepository;

    public function __construct(OfertaRepository $ofertaRepo)
    {
        $this->ofertaRepository = $ofertaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ofertas",
     *      summary="Get a listing of the Ofertas.",
     *      tags={"Oferta"},
     *      description="Get all Ofertas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Oferta")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ofertas = $this->ofertaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ofertas->toArray(), 'Ofertas retrieved successfully');
    }

    /**
     * @param CreateOfertaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ofertas",
     *      summary="Store a newly created Oferta in storage",
     *      tags={"Oferta"},
     *      description="Store Oferta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Oferta that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Oferta")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Oferta"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOfertaAPIRequest $request)
    {
        $input = $request->all();

        $oferta = $this->ofertaRepository->create($input);

        return $this->sendResponse($oferta->toArray(), 'Oferta saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ofertas/{id}",
     *      summary="Display the specified Oferta",
     *      tags={"Oferta"},
     *      description="Get Oferta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Oferta",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Oferta"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Oferta $oferta */
        $oferta = $this->ofertaRepository->find($id);

        if (empty($oferta)) {
            return $this->sendError('Oferta not found');
        }

        return $this->sendResponse($oferta->toArray(), 'Oferta retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOfertaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ofertas/{id}",
     *      summary="Update the specified Oferta in storage",
     *      tags={"Oferta"},
     *      description="Update Oferta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Oferta",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Oferta that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Oferta")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Oferta"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOfertaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Oferta $oferta */
        $oferta = $this->ofertaRepository->find($id);

        if (empty($oferta)) {
            return $this->sendError('Oferta not found');
        }

        $oferta = $this->ofertaRepository->update($input, $id);

        return $this->sendResponse($oferta->toArray(), 'Oferta updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ofertas/{id}",
     *      summary="Remove the specified Oferta from storage",
     *      tags={"Oferta"},
     *      description="Delete Oferta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Oferta",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Oferta $oferta */
        $oferta = $this->ofertaRepository->find($id);

        if (empty($oferta)) {
            return $this->sendError('Oferta not found');
        }

        $oferta->delete();

        return $this->sendSuccess('Oferta deleted successfully');
    }
}
