<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAdministrativoAPIRequest;
use App\Http\Requests\API\UpdateAdministrativoAPIRequest;
use App\Models\Administrativo;
use App\Repositories\AdministrativoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AdministrativoController
 * @package App\Http\Controllers\API
 */

class AdministrativoAPIController extends AppBaseController
{
    /** @var  AdministrativoRepository */
    private $administrativoRepository;

    public function __construct(AdministrativoRepository $administrativoRepo)
    {
        $this->administrativoRepository = $administrativoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/administrativos",
     *      summary="Get a listing of the Administrativos.",
     *      tags={"Administrativo"},
     *      description="Get all Administrativos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Administrativo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $administrativos = $this->administrativoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($administrativos->toArray(), 'Administrativos retrieved successfully');
    }

    /**
     * @param CreateAdministrativoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/administrativos",
     *      summary="Store a newly created Administrativo in storage",
     *      tags={"Administrativo"},
     *      description="Store Administrativo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Administrativo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Administrativo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Administrativo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAdministrativoAPIRequest $request)
    {
        $input = $request->all();

        $administrativo = $this->administrativoRepository->create($input);

        return $this->sendResponse($administrativo->toArray(), 'Administrativo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/administrativos/{id}",
     *      summary="Display the specified Administrativo",
     *      tags={"Administrativo"},
     *      description="Get Administrativo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Administrativo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Administrativo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Administrativo $administrativo */
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            return $this->sendError('Administrativo not found');
        }

        return $this->sendResponse($administrativo->toArray(), 'Administrativo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAdministrativoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/administrativos/{id}",
     *      summary="Update the specified Administrativo in storage",
     *      tags={"Administrativo"},
     *      description="Update Administrativo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Administrativo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Administrativo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Administrativo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Administrativo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAdministrativoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Administrativo $administrativo */
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            return $this->sendError('Administrativo not found');
        }

        $administrativo = $this->administrativoRepository->update($input, $id);

        return $this->sendResponse($administrativo->toArray(), 'Administrativo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/administrativos/{id}",
     *      summary="Remove the specified Administrativo from storage",
     *      tags={"Administrativo"},
     *      description="Delete Administrativo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Administrativo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Administrativo $administrativo */
        $administrativo = $this->administrativoRepository->find($id);

        if (empty($administrativo)) {
            return $this->sendError('Administrativo not found');
        }

        $administrativo->delete();

        return $this->sendSuccess('Administrativo deleted successfully');
    }
}
