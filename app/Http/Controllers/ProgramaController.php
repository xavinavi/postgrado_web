<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProgramaRequest;
use App\Http\Requests\UpdateProgramaRequest;
use App\Repositories\ProgramaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Periodo;
use App\Models\Categoria;
use App\Models\Modulo;
class ProgramaController extends AppBaseController
{
    /** @var  ProgramaRepository */
    private $programaRepository;

    public function __construct(ProgramaRepository $programaRepo)
    {
        $this->programaRepository = $programaRepo;
    }

    /**
     * Display a listing of the Programa.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $programas = $this->programaRepository->paginate(10);

        return view('programas.index')
            ->with('programas', $programas);
    }

    /**
     * Show the form for creating a new Programa.
     *
     * @return Response
     */
    public function create()
    {

        $periodos=Periodo::all();
        $categorias=Categoria::all();
        return view('programas.create',compact('periodos','categorias'));
    }

    /**
     * Store a newly created Programa in storage.
     *
     * @param CreateProgramaRequest $request
     *
     * @return Response
     */
    public function store(CreateProgramaRequest $request)
    {

        $input = $request->all();

        $programa = $this->programaRepository->create($input);

        Flash::success('Programa saved successfully.');

        return redirect(route('programas.index'));
    }

    /**
     * Display the specified Programa.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            Flash::error('Programa not found');

            return redirect(route('programas.index'));
        }

        return view('programas.show')->with('programa', $programa);
    }




    public function programaModulo($id)
    {
        $modulos = Modulo::where('programa_id',$id)->paginate(10);

        if (empty($modulos)) {
            Flash::error('Programa no tiene modulo not found');

            return redirect(route('programas.index'));
        }

        return view('modulos.index',compact('modulos'));
    }

    /**
     * Show the form for editing the specified Programa.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            Flash::error('Programa not found');

            return redirect(route('programas.index'));
        }
        $periodos=Periodo::all();
        $categorias=Categoria::all();
        return view('programas.edit',compact('programa','periodos','categorias'));

      //  return view('programas.edit')->with('programa', $programa);
    }

    /**
     * Update the specified Programa in storage.
     *
     * @param int $id
     * @param UpdateProgramaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProgramaRequest $request)
    {
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            Flash::error('Programa not found');

            return redirect(route('programas.index'));
        }

        $programa = $this->programaRepository->update($request->all(), $id);

        Flash::success('Programa updated successfully.');

        return redirect(route('programas.index'));
    }

    /**
     * Remove the specified Programa from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $programa = $this->programaRepository->find($id);

        if (empty($programa)) {
            Flash::error('Programa not found');

            return redirect(route('programas.index'));
        }

        $this->programaRepository->delete($id);

        Flash::success('Programa deleted successfully.');

        return redirect(route('programas.index'));
    }
}
