<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfesorRequest;
use App\Http\Requests\UpdateProfesorRequest;
use App\Repositories\ProfesorRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Persona;
use App\Models\Profesor;
use Illuminate\Http\Request;
use Flash;
use Response;
use Illuminate\Support\Facades\redirect;

class ProfesorController extends AppBaseController
{
    /** @var  ProfesorRepository */
    private $profesorRepository;

    public function __construct(ProfesorRepository $profesorRepo)
    {
        $this->profesorRepository = $profesorRepo;
    }

    /**
     * Display a listing of the Profesor.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $profesors = Profesor::join('personas', 'profesores.persona_id', 'personas.id')
            ->select('ci', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'direccion','genero', 'profesion', 'profesores.id')
            ->paginate(10);

        return view('profesors.index')->with('profesors', $profesors);
    }

    /**
     * Show the form for creating a new Profesor.
     *
     * @return Response
     */
    public function create()
    {
        return view('profesors.create');
    }

    /**
     * Store a newly created Profesor in storage.
     *
     * @param CreateProfesorRequest $request
     *
     * @return Response
     */
    public function store(CreateProfesorRequest $request)
    {

        $this->validate($request, [
            'nombre' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'ci' => 'required',
            'telefono' => 'required',
            'genero' => 'required',
            'direccion' => 'required'
        ]);
        $persona = new Persona();

        $persona->ci = $request->ci;
        $persona->nombre = $request->nombre;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->genero = $request->genero;

        $persona->save();

        $profesor = new Profesor();

        $profesor->profesion = $request->profesion;
        $profesor->persona_id = $persona->id;

        $profesor->save();

        Flash::success('Profesor saved successfully.');

        return redirect(route('profesors.index'));
    }

    /**
     * Display the specified Profesor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            Flash::error('Profesor not found');

            return redirect(route('profesors.index'));
        }

        return view('profesors.show')->with('profesor', $profesor);
    }

    /**
     * Show the form for editing the specified Profesor.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {


        $profesor = Profesor::join('personas', 'profesores.persona_id', 'personas.id')
        ->where('profesores.id' , $id)
        ->select('ci', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'direccion','genero', 'profesion', 'profesores.id')
        ->get();

        if (empty($profesor)) {
            Flash::error('Profesor not found');

            return redirect(route('profesors.index'));
        }

        return view('profesors.edit')->with('profesor', $profesor[0]);
    }

    /**
     * Update the specified Profesor in storage.
     *
     * @param int $id
     * @param UpdateProfesorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfesorRequest $request)
    {

        $this->validate($request, [
            'nombre' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'ci' => 'required',
            'telefono' => 'required',
            'genero' => 'required',
            'direccion' => 'required'
        ]);
        $profesor = Profesor::findOrFail($id);

        $profesor->profesion = $request->profesion;
        $profesor->update();

        $persona = Persona::findOrFail($profesor->persona_id);

        $persona->ci = $request->ci;
        $persona->nombre = $request->nombre;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->genero = $request->genero;

        $persona->update();

        Flash::success('Profesor updated successfully.');

        return redirect(route('profesors.index'));
    }

    /**
     * Remove the specified Profesor from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profesor = $this->profesorRepository->find($id);

        if (empty($profesor)) {
            Flash::error('Profesor not found');

            return redirect(route('profesors.index'));
        }

        $this->profesorRepository->delete($id);

        Flash::success('Profesor deleted successfully.');

        return redirect(route('profesors.index'));
    }
}
