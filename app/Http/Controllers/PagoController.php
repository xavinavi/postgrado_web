<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePagoRequest;
use App\Http\Requests\UpdatePagoRequest;
use App\Repositories\PagoRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Inscripcion;
use App\Models\Pago;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;
class PagoController extends AppBaseController
{
    /** @var  PagoRepository */
    private $pagoRepository;

    public function __construct(PagoRepository $pagoRepo)
    {
        $this->pagoRepository = $pagoRepo;
    }

    /**
     * Display a listing of the Pago.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $codigo=$request->get('codigo');
        $estudiante=$request->get('estudiante');
        $estado=$request->get('estado');
        $now = new \DateTime();
        $fecha = DB::table('pagos')->orderBy('fecha', 'asc')->first();
        if($fecha){
            $fecha=array($fecha->fecha ,$now->format('Y-m-d H:i:s'));
        }else{
            $fecha=array($now->format('Y-m-d H:i:s'),$now->format('Y-m-d H:i:s'));
        }
        $pago=null;
        if ($request->get('estado') && $request->get('estado')=='Por Pagar'){
            $pago='>';
        }elseif ($request->get('estado') && $request->get('estado')=='saldo Cancelado'){
            $pago='<';
        }

        if($request->get('fecha') && $request->get('estado'))
        {
            $fecha = explode("to", $request->get('fecha'));
            $pagos=Pago::orderBy('pagos.id', 'DESC')
                ->join('inscripcion', 'inscripcion.id', '=', 'pagos.inscripcion_id')
                ->join('estudiantes', 'estudiantes.id', '=', 'inscripcion.estudiante_id')
                ->join('personas', 'personas.id', '=', 'estudiantes.persona_id')
                ->where('personas.nombre','LIKE',"%$estudiante%")
                ->where('pagos.saldo',$pago,0)
                ->where('pagos.id','LIKE',"%$codigo%")
                ->whereBetween('pagos.fecha',[$fecha[0],$fecha[1]])
                ->select('pagos.*')
                ->paginate(10)->setPath ( '' );



        }else{
            $pagos=Pago::orderBy('pagos.id', 'DESC')
                ->join('inscripcion', 'inscripcion.id', '=', 'pagos.inscripcion_id')
                ->join('estudiantes', 'estudiantes.id', '=', 'inscripcion.estudiante_id')
                ->join('personas', 'personas.id', '=', 'estudiantes.persona_id')
                ->where('personas.nombre','LIKE',"%$estudiante%")
                ->where('pagos.id','LIKE',"%$codigo%")
                ->paginate(10)->setPath ( '' );

        }

        $pagination = $pagos->appends ( array (
            'codigo' => $request->get( 'codigo' ),
            'estudiante' => $request->get( 'estudiante' ),
            'fecha' => $request->get( 'fecha' ),
            'estado' => $request->get( 'estado' )
        ) );

       // $pagos = $this->pagoRepository->paginate(10);
        return view('pagos.index', compact('pagos','codigo','estudiante','fecha','estado'));

    }

    /**
     * Show the form for creating a new Pago.
     *
     * @return Response
     */
    public function create()
    {
        return view('pagos.create');
    }

    /**
     * Store a newly created Pago in storage.
     *
     * @param CreatePagoRequest $request
     *
     * @return Response
     */
    public function store(CreatePagoRequest $request)
    {
        $input = $request->all();

        $pago = $this->pagoRepository->create($input);

        Flash::success('Pago saved successfully.');

        return redirect(route('pagar.inscripcion'));
    }

    /**
     * Display the specified Pago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        return view('pagos.show')->with('pago', $pago);
    }

    /**
     * Show the form for editing the specified Pago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        return view('pagos.edit')->with('pago', $pago);
    }

    /**
     * Update the specified Pago in storage.
     *
     * @param int $id
     * @param UpdatePagoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePagoRequest $request)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        $pago = $this->pagoRepository->update($request->all(), $id);

        Flash::success('Pago updated successfully.');

        return redirect(route('pagos.index'));
    }

    /**
     * Remove the specified Pago from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            Flash::error('Pago not found');

            return redirect(route('pagos.index'));
        }

        $this->pagoRepository->delete($id);

        Flash::success('Pago deleted successfully.');

        return redirect(route('pagos.index'));
    }

    public function realizarPagoReserva($id)
    {
        $inscripcion =Inscripcion::findOrFail($id);

        return view('pagos.realizapago', compact('inscripcion'));
    }

    public function realizarPagoInscripcion($id)
    {
        $inscripcion =Inscripcion::findOrFail($id);

        return view('pagos.realizapago', compact('inscripcion'));
    }
    public function realizarPagoSaldoInscripcion($id)
    {
        $inscripcion =Inscripcion::findOrFail($id);

        return view('pagos.pagarSaldo', compact('inscripcion'));
    }

    public function confirmarPago(Request $request, $id)
    {
        $request->validate([
            'monto' => 'required|numeric|min:0',
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);
        $pago = new Pago();

        $pago->fecha = date('Y-m-d');
        $pago->monto = $request->monto;
        $pago->nombre = $request->nombre;
        $pago->descripcion = $request->descripcion;
        $pago->inscripcion_id = $id;

        $inscripcion = Inscripcion::find($id);
        //dd( $inscripcion->modulos->sum('costo') - $request->monto);
        $pago->saldo = $inscripcion->modulos->sum('costo') - $request->monto;

        $pago->save();

        DB::table('inscripcion_pago')->insert(
            ['pago_id' => $pago->id,
                'inscripcion_id' => $id,
                'pago' =>$request->input('monto'),
                'fecha' => date('Y-m-d')
            ]
        );
        return redirect(route('pagos.show',$pago->id)); /// modificar
    }
  public function confirmarPagoSaldoInscripcion(Request $request, $id)
    {
        $request->validate([
            'monto' => 'required|numeric|min:0',

        ]);
        //id es de ala inscripcion
        $pago = Pago::where('inscripcion_id',$id)->get();

        $pago=Pago::find($pago[0]->id);

        $inscripcion = Inscripcion::find($id);

    DB::table('inscripcion_pago')->insert(
            ['pago_id' => $pago->id,
                'inscripcion_id' => $id,
                'pago' =>$request->input('monto'),
                'fecha' => date('Y-m-d')
            ]
        );


        $pagosCancelados =DB::table('inscripcion_pago')->where('inscripcion_id',$id)->sum('pago');

        $pago->saldo =$inscripcion->modulos->sum('costo') - $pagosCancelados;
        $pago->monto=$pagosCancelados;

        $pago->update();

        return redirect(route('pagos.index'));
    }

    public function confirmarPagoInscripcion(Request $request, $id)
    {
        $request->validate([
            'monto' => 'required|numeric|min:0',
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);

        $pago = new Pago();

        $pago->fecha = date('Y-m-d');
        $pago->monto = $request->monto;
        $pago->nombre = $request->nombre;
        $pago->descripcion = $request->descripcion;
        $pago->inscripcion_id = $id;

        $inscripcion = Inscripcion::find($id);
        //dd( $inscripcion->modulos->sum('costo') - $request->monto);
        $pago->saldo = $inscripcion->modulos->sum('costo') - $request->monto;

        $pago->save();

        return redirect(route('pagos.index'));
    }
}
