<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGestionRequest;
use App\Http\Requests\UpdateGestionRequest;
use App\Repositories\GestionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GestionController extends AppBaseController
{
    /** @var  GestionRepository */
    private $gestionRepository;

    public function __construct(GestionRepository $gestionRepo)
    {
        $this->gestionRepository = $gestionRepo;
    }

    /**
     * Display a listing of the Gestion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $gestions = $this->gestionRepository->paginate(10);

        return view('gestions.index')
            ->with('gestions', $gestions);
    }

    /**
     * Show the form for creating a new Gestion.
     *
     * @return Response
     */
    public function create()
    {
        return view('gestions.create');
    }

    /**
     * Store a newly created Gestion in storage.
     *
     * @param CreateGestionRequest $request
     *
     * @return Response
     */
    public function store(CreateGestionRequest $request)
    {
        $input = $request->all();

        $gestion = $this->gestionRepository->create($input);

        Flash::success('Gestion saved successfully.');

        return redirect(route('gestions.index'));
    }

    /**
     * Display the specified Gestion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            Flash::error('Gestion not found');

            return redirect(route('gestions.index'));
        }

        return view('gestions.show')->with('gestion', $gestion);
    }

    /**
     * Show the form for editing the specified Gestion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            Flash::error('Gestion not found');

            return redirect(route('gestions.index'));
        }

        return view('gestions.edit')->with('gestion', $gestion);
    }

    /**
     * Update the specified Gestion in storage.
     *
     * @param int $id
     * @param UpdateGestionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGestionRequest $request)
    {
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            Flash::error('Gestion not found');

            return redirect(route('gestions.index'));
        }

        $gestion = $this->gestionRepository->update($request->all(), $id);

        Flash::success('Gestion updated successfully.');

        return redirect(route('gestions.index'));
    }

    /**
     * Remove the specified Gestion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gestion = $this->gestionRepository->find($id);

        if (empty($gestion)) {
            Flash::error('Gestion not found');

            return redirect(route('gestions.index'));
        }

        $this->gestionRepository->delete($id);

        Flash::success('Gestion deleted successfully.');

        return redirect(route('gestions.index'));
    }
}
