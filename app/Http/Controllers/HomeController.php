<?php

namespace App\Http\Controllers;
use Flash;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Spatie\Searchable\Search;

use App\Models\User;
use App\Models\Administrativo;
use App\Models\Categoria;
use App\Models\Estudiante;
use App\Models\Gestion;
use App\Models\Inscripcion;
use App\Models\Modulo;
use App\Models\Oferta;
use App\Models\Pago;
use App\Models\Periodo;
use App\Models\Persona;
use App\Models\Profesor;
use App\Models\Programa;
use App\Models\Reserva;
use App\Models\Agenda;

use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //   $datos = DB::table('home_modulofecha')->get();


        return view('home');
    }

    /***
       busqueda generica de toda la pagina web
     **/

    public function search(Request $request)
    {
        $query = $request->input('query');
        if (empty($query)) {
            Flash::warning('por favor ingrese la palabra a buscar');
            $searchResults="";
            return view('search', compact('searchResults'));
        }
        $searchResults = (new Search())
         //   ->registerModel(Paciente::class, 'apellido_paterno','apellido_materno')

            ->registerModel(User::class, 'name','email')
            ->registerModel(Modulo::class, 'nombre')
            ->registerModel(Categoria::class, 'nombre','descripcion')
            ->registerModel(Oferta::class, 'nombre','descripcion')
            ->registerModel(Gestion::class,'descripcion')

            ->registerModel(Administrativo::class, 'cargo')
            ->registerModel(Programa::class, 'nombre','duracion')

            /*    ->registerModel(Estudiante::class, 'registro')

                 ->registerModel(Inscripcion::class, 'fecha')
                 ->registerModel(Modulo::class, 'nombre','fecha_inicio','fecha_fin','hora_inicio','hora_salida','costo','descripcion','programa_id','profesor_id')
             ->registerModel(Oferta::class, 'name','email')
                 ->registerModel(Pago::class, 'name','email')
                 ->registerModel(Periodo::class, 'name','email')
                 ->registerModel(Persona::class, 'name','email')
                 ->registerModel(Profesor::class, 'name','email')
                 ->registerModel(Programa::class, 'name','email')
                 ->registerModel(Reserva::class, 'name','email')
     */
            ->perform($query);

        if ($searchResults->count()>0){
            Flash::success('Busqueda realizada exitosamente.');

        }else{
            Flash::warning('No se encontraron datos para su busqueda.');

        }
        return view('search', compact('searchResults'));
    }

    /*****
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     *   crear estilos
     */

    public  function estilosCreate(){
        return view('estilos');

    }



    public  function estilosStoreTemplete(Request $request, $id){
        $user=User::find(Auth::id());
        $estiloTemplete = array( $id, null,null );
        $user->estilo=serialize( $estiloTemplete );
        $user->update();
        Flash::success('Estilo Modificado exitosamente.');
        return redirect()->back();
    }


    public  function estilosStoregeFont(Request $request){
        $user=User::find(Auth::id());
        $estiloTemplete= $user->estilo;
        if($estiloTemplete){
            $añadirEstilo=unserialize( $user->estilo );
            $añadirEstilo[1]=$request->input('font-family');
            $añadirEstilo[2]=$request->input('font-size');
            $user->estilo=serialize($añadirEstilo);
        }else{
            $añadirEstilo= array( 'skin-blue sidebar-mini', null,null );;
            $añadirEstilo[1]=$request->input('font-family');
            $añadirEstilo[2]=$request->input('font-size');
            $user->estilo=serialize($añadirEstilo);
        }
        $user->update();
        Flash::success('Estilo Modificado exitosamente.');
        return redirect()->back();
    }



    public  function estilosRestablecer(){
        $user=User::find(Auth::id());
        $user->estilo=null;
        $user->update();
        Flash::success('Estilo Restaurado exitosamente.');
        return redirect()->back();

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *   VISITAS
     */
     public  function vistaStore(Request $request){
        $pathUrl=$request->pathUrl;
         $tableUrl = DB::table('urls')->where('nombre',$pathUrl)->first();
         if(!$tableUrl){
             $tableUrl=   DB::table('urls')->insertGetId(['nombre' =>$pathUrl]);
             DB::table('visitas')->insert(['url_id' => $tableUrl, 'user_id' => Auth::id(),'cantidad'=>1]);

             $visitaPahUrl = DB::table('visitas')->where('url_id',$tableUrl)->sum('cantidad');
             return response()->json(['visitaPahUrl'=>$visitaPahUrl]);
         }

          DB::table('visitas')
             ->where('url_id', '=', $tableUrl->id)
             ->where('user_id', '=', Auth::id())
             ->increment('cantidad');

         $visitaPahUrl = DB::table('visitas')->where('url_id',$tableUrl->id)->sum('cantidad');

         return response()->json(['visitaPahUrl'=>$visitaPahUrl]);

    }






}
