<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Gestion;

class GestionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_gestion()
    {
        $gestion = factory(Gestion::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/gestions', $gestion
        );

        $this->assertApiResponse($gestion);
    }

    /**
     * @test
     */
    public function test_read_gestion()
    {
        $gestion = factory(Gestion::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/gestions/'.$gestion->id
        );

        $this->assertApiResponse($gestion->toArray());
    }

    /**
     * @test
     */
    public function test_update_gestion()
    {
        $gestion = factory(Gestion::class)->create();
        $editedGestion = factory(Gestion::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/gestions/'.$gestion->id,
            $editedGestion
        );

        $this->assertApiResponse($editedGestion);
    }

    /**
     * @test
     */
    public function test_delete_gestion()
    {
        $gestion = factory(Gestion::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/gestions/'.$gestion->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/gestions/'.$gestion->id
        );

        $this->response->assertStatus(404);
    }
}
