<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Profesor;

class ProfesorApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_profesor()
    {
        $profesor = factory(Profesor::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/profesors', $profesor
        );

        $this->assertApiResponse($profesor);
    }

    /**
     * @test
     */
    public function test_read_profesor()
    {
        $profesor = factory(Profesor::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/profesors/'.$profesor->id
        );

        $this->assertApiResponse($profesor->toArray());
    }

    /**
     * @test
     */
    public function test_update_profesor()
    {
        $profesor = factory(Profesor::class)->create();
        $editedProfesor = factory(Profesor::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/profesors/'.$profesor->id,
            $editedProfesor
        );

        $this->assertApiResponse($editedProfesor);
    }

    /**
     * @test
     */
    public function test_delete_profesor()
    {
        $profesor = factory(Profesor::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/profesors/'.$profesor->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/profesors/'.$profesor->id
        );

        $this->response->assertStatus(404);
    }
}
