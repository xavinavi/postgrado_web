<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Periodo;

class PeriodoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_periodo()
    {
        $periodo = factory(Periodo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/periodos', $periodo
        );

        $this->assertApiResponse($periodo);
    }

    /**
     * @test
     */
    public function test_read_periodo()
    {
        $periodo = factory(Periodo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/periodos/'.$periodo->id
        );

        $this->assertApiResponse($periodo->toArray());
    }

    /**
     * @test
     */
    public function test_update_periodo()
    {
        $periodo = factory(Periodo::class)->create();
        $editedPeriodo = factory(Periodo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/periodos/'.$periodo->id,
            $editedPeriodo
        );

        $this->assertApiResponse($editedPeriodo);
    }

    /**
     * @test
     */
    public function test_delete_periodo()
    {
        $periodo = factory(Periodo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/periodos/'.$periodo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/periodos/'.$periodo->id
        );

        $this->response->assertStatus(404);
    }
}
