<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Oferta;

class OfertaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_oferta()
    {
        $oferta = factory(Oferta::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ofertas', $oferta
        );

        $this->assertApiResponse($oferta);
    }

    /**
     * @test
     */
    public function test_read_oferta()
    {
        $oferta = factory(Oferta::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/ofertas/'.$oferta->id
        );

        $this->assertApiResponse($oferta->toArray());
    }

    /**
     * @test
     */
    public function test_update_oferta()
    {
        $oferta = factory(Oferta::class)->create();
        $editedOferta = factory(Oferta::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ofertas/'.$oferta->id,
            $editedOferta
        );

        $this->assertApiResponse($editedOferta);
    }

    /**
     * @test
     */
    public function test_delete_oferta()
    {
        $oferta = factory(Oferta::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ofertas/'.$oferta->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ofertas/'.$oferta->id
        );

        $this->response->assertStatus(404);
    }
}
