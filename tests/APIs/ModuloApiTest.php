<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Modulo;

class ModuloApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_modulo()
    {
        $modulo = factory(Modulo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/modulos', $modulo
        );

        $this->assertApiResponse($modulo);
    }

    /**
     * @test
     */
    public function test_read_modulo()
    {
        $modulo = factory(Modulo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/modulos/'.$modulo->id
        );

        $this->assertApiResponse($modulo->toArray());
    }

    /**
     * @test
     */
    public function test_update_modulo()
    {
        $modulo = factory(Modulo::class)->create();
        $editedModulo = factory(Modulo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/modulos/'.$modulo->id,
            $editedModulo
        );

        $this->assertApiResponse($editedModulo);
    }

    /**
     * @test
     */
    public function test_delete_modulo()
    {
        $modulo = factory(Modulo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/modulos/'.$modulo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/modulos/'.$modulo->id
        );

        $this->response->assertStatus(404);
    }
}
