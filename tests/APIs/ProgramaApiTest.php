<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Programa;

class ProgramaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_programa()
    {
        $programa = factory(Programa::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/programas', $programa
        );

        $this->assertApiResponse($programa);
    }

    /**
     * @test
     */
    public function test_read_programa()
    {
        $programa = factory(Programa::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/programas/'.$programa->id
        );

        $this->assertApiResponse($programa->toArray());
    }

    /**
     * @test
     */
    public function test_update_programa()
    {
        $programa = factory(Programa::class)->create();
        $editedPrograma = factory(Programa::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/programas/'.$programa->id,
            $editedPrograma
        );

        $this->assertApiResponse($editedPrograma);
    }

    /**
     * @test
     */
    public function test_delete_programa()
    {
        $programa = factory(Programa::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/programas/'.$programa->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/programas/'.$programa->id
        );

        $this->response->assertStatus(404);
    }
}
