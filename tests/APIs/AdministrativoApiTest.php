<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Administrativo;

class AdministrativoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_administrativo()
    {
        $administrativo = factory(Administrativo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/administrativos', $administrativo
        );

        $this->assertApiResponse($administrativo);
    }

    /**
     * @test
     */
    public function test_read_administrativo()
    {
        $administrativo = factory(Administrativo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/administrativos/'.$administrativo->id
        );

        $this->assertApiResponse($administrativo->toArray());
    }

    /**
     * @test
     */
    public function test_update_administrativo()
    {
        $administrativo = factory(Administrativo::class)->create();
        $editedAdministrativo = factory(Administrativo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/administrativos/'.$administrativo->id,
            $editedAdministrativo
        );

        $this->assertApiResponse($editedAdministrativo);
    }

    /**
     * @test
     */
    public function test_delete_administrativo()
    {
        $administrativo = factory(Administrativo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/administrativos/'.$administrativo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/administrativos/'.$administrativo->id
        );

        $this->response->assertStatus(404);
    }
}
