<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Inscripcion;

class InscripcionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/inscripcions', $inscripcion
        );

        $this->assertApiResponse($inscripcion);
    }

    /**
     * @test
     */
    public function test_read_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/inscripcions/'.$inscripcion->id
        );

        $this->assertApiResponse($inscripcion->toArray());
    }

    /**
     * @test
     */
    public function test_update_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->create();
        $editedInscripcion = factory(Inscripcion::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/inscripcions/'.$inscripcion->id,
            $editedInscripcion
        );

        $this->assertApiResponse($editedInscripcion);
    }

    /**
     * @test
     */
    public function test_delete_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/inscripcions/'.$inscripcion->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/inscripcions/'.$inscripcion->id
        );

        $this->response->assertStatus(404);
    }
}
