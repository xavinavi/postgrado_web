<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Estudiante;

class EstudianteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_estudiante()
    {
        $estudiante = factory(Estudiante::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/estudiantes', $estudiante
        );

        $this->assertApiResponse($estudiante);
    }

    /**
     * @test
     */
    public function test_read_estudiante()
    {
        $estudiante = factory(Estudiante::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/estudiantes/'.$estudiante->id
        );

        $this->assertApiResponse($estudiante->toArray());
    }

    /**
     * @test
     */
    public function test_update_estudiante()
    {
        $estudiante = factory(Estudiante::class)->create();
        $editedEstudiante = factory(Estudiante::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/estudiantes/'.$estudiante->id,
            $editedEstudiante
        );

        $this->assertApiResponse($editedEstudiante);
    }

    /**
     * @test
     */
    public function test_delete_estudiante()
    {
        $estudiante = factory(Estudiante::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/estudiantes/'.$estudiante->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/estudiantes/'.$estudiante->id
        );

        $this->response->assertStatus(404);
    }
}
