<?php namespace Tests\Repositories;

use App\Models\Modulo;
use App\Repositories\ModuloRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ModuloRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ModuloRepository
     */
    protected $moduloRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->moduloRepo = \App::make(ModuloRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_modulo()
    {
        $modulo = factory(Modulo::class)->make()->toArray();

        $createdModulo = $this->moduloRepo->create($modulo);

        $createdModulo = $createdModulo->toArray();
        $this->assertArrayHasKey('id', $createdModulo);
        $this->assertNotNull($createdModulo['id'], 'Created Modulo must have id specified');
        $this->assertNotNull(Modulo::find($createdModulo['id']), 'Modulo with given id must be in DB');
        $this->assertModelData($modulo, $createdModulo);
    }

    /**
     * @test read
     */
    public function test_read_modulo()
    {
        $modulo = factory(Modulo::class)->create();

        $dbModulo = $this->moduloRepo->find($modulo->id);

        $dbModulo = $dbModulo->toArray();
        $this->assertModelData($modulo->toArray(), $dbModulo);
    }

    /**
     * @test update
     */
    public function test_update_modulo()
    {
        $modulo = factory(Modulo::class)->create();
        $fakeModulo = factory(Modulo::class)->make()->toArray();

        $updatedModulo = $this->moduloRepo->update($fakeModulo, $modulo->id);

        $this->assertModelData($fakeModulo, $updatedModulo->toArray());
        $dbModulo = $this->moduloRepo->find($modulo->id);
        $this->assertModelData($fakeModulo, $dbModulo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_modulo()
    {
        $modulo = factory(Modulo::class)->create();

        $resp = $this->moduloRepo->delete($modulo->id);

        $this->assertTrue($resp);
        $this->assertNull(Modulo::find($modulo->id), 'Modulo should not exist in DB');
    }
}
