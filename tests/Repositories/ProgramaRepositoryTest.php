<?php namespace Tests\Repositories;

use App\Models\Programa;
use App\Repositories\ProgramaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProgramaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProgramaRepository
     */
    protected $programaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->programaRepo = \App::make(ProgramaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_programa()
    {
        $programa = factory(Programa::class)->make()->toArray();

        $createdPrograma = $this->programaRepo->create($programa);

        $createdPrograma = $createdPrograma->toArray();
        $this->assertArrayHasKey('id', $createdPrograma);
        $this->assertNotNull($createdPrograma['id'], 'Created Programa must have id specified');
        $this->assertNotNull(Programa::find($createdPrograma['id']), 'Programa with given id must be in DB');
        $this->assertModelData($programa, $createdPrograma);
    }

    /**
     * @test read
     */
    public function test_read_programa()
    {
        $programa = factory(Programa::class)->create();

        $dbPrograma = $this->programaRepo->find($programa->id);

        $dbPrograma = $dbPrograma->toArray();
        $this->assertModelData($programa->toArray(), $dbPrograma);
    }

    /**
     * @test update
     */
    public function test_update_programa()
    {
        $programa = factory(Programa::class)->create();
        $fakePrograma = factory(Programa::class)->make()->toArray();

        $updatedPrograma = $this->programaRepo->update($fakePrograma, $programa->id);

        $this->assertModelData($fakePrograma, $updatedPrograma->toArray());
        $dbPrograma = $this->programaRepo->find($programa->id);
        $this->assertModelData($fakePrograma, $dbPrograma->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_programa()
    {
        $programa = factory(Programa::class)->create();

        $resp = $this->programaRepo->delete($programa->id);

        $this->assertTrue($resp);
        $this->assertNull(Programa::find($programa->id), 'Programa should not exist in DB');
    }
}
