<?php namespace Tests\Repositories;

use App\Models\Profesor;
use App\Repositories\ProfesorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProfesorRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProfesorRepository
     */
    protected $profesorRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->profesorRepo = \App::make(ProfesorRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_profesor()
    {
        $profesor = factory(Profesor::class)->make()->toArray();

        $createdProfesor = $this->profesorRepo->create($profesor);

        $createdProfesor = $createdProfesor->toArray();
        $this->assertArrayHasKey('id', $createdProfesor);
        $this->assertNotNull($createdProfesor['id'], 'Created Profesor must have id specified');
        $this->assertNotNull(Profesor::find($createdProfesor['id']), 'Profesor with given id must be in DB');
        $this->assertModelData($profesor, $createdProfesor);
    }

    /**
     * @test read
     */
    public function test_read_profesor()
    {
        $profesor = factory(Profesor::class)->create();

        $dbProfesor = $this->profesorRepo->find($profesor->id);

        $dbProfesor = $dbProfesor->toArray();
        $this->assertModelData($profesor->toArray(), $dbProfesor);
    }

    /**
     * @test update
     */
    public function test_update_profesor()
    {
        $profesor = factory(Profesor::class)->create();
        $fakeProfesor = factory(Profesor::class)->make()->toArray();

        $updatedProfesor = $this->profesorRepo->update($fakeProfesor, $profesor->id);

        $this->assertModelData($fakeProfesor, $updatedProfesor->toArray());
        $dbProfesor = $this->profesorRepo->find($profesor->id);
        $this->assertModelData($fakeProfesor, $dbProfesor->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_profesor()
    {
        $profesor = factory(Profesor::class)->create();

        $resp = $this->profesorRepo->delete($profesor->id);

        $this->assertTrue($resp);
        $this->assertNull(Profesor::find($profesor->id), 'Profesor should not exist in DB');
    }
}
