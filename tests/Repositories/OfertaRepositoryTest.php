<?php namespace Tests\Repositories;

use App\Models\Oferta;
use App\Repositories\OfertaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OfertaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OfertaRepository
     */
    protected $ofertaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ofertaRepo = \App::make(OfertaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_oferta()
    {
        $oferta = factory(Oferta::class)->make()->toArray();

        $createdOferta = $this->ofertaRepo->create($oferta);

        $createdOferta = $createdOferta->toArray();
        $this->assertArrayHasKey('id', $createdOferta);
        $this->assertNotNull($createdOferta['id'], 'Created Oferta must have id specified');
        $this->assertNotNull(Oferta::find($createdOferta['id']), 'Oferta with given id must be in DB');
        $this->assertModelData($oferta, $createdOferta);
    }

    /**
     * @test read
     */
    public function test_read_oferta()
    {
        $oferta = factory(Oferta::class)->create();

        $dbOferta = $this->ofertaRepo->find($oferta->id);

        $dbOferta = $dbOferta->toArray();
        $this->assertModelData($oferta->toArray(), $dbOferta);
    }

    /**
     * @test update
     */
    public function test_update_oferta()
    {
        $oferta = factory(Oferta::class)->create();
        $fakeOferta = factory(Oferta::class)->make()->toArray();

        $updatedOferta = $this->ofertaRepo->update($fakeOferta, $oferta->id);

        $this->assertModelData($fakeOferta, $updatedOferta->toArray());
        $dbOferta = $this->ofertaRepo->find($oferta->id);
        $this->assertModelData($fakeOferta, $dbOferta->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_oferta()
    {
        $oferta = factory(Oferta::class)->create();

        $resp = $this->ofertaRepo->delete($oferta->id);

        $this->assertTrue($resp);
        $this->assertNull(Oferta::find($oferta->id), 'Oferta should not exist in DB');
    }
}
