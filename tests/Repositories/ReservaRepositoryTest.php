<?php namespace Tests\Repositories;

use App\Models\Reserva;
use App\Repositories\ReservaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ReservaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ReservaRepository
     */
    protected $reservaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->reservaRepo = \App::make(ReservaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_reserva()
    {
        $reserva = factory(Reserva::class)->make()->toArray();

        $createdReserva = $this->reservaRepo->create($reserva);

        $createdReserva = $createdReserva->toArray();
        $this->assertArrayHasKey('id', $createdReserva);
        $this->assertNotNull($createdReserva['id'], 'Created Reserva must have id specified');
        $this->assertNotNull(Reserva::find($createdReserva['id']), 'Reserva with given id must be in DB');
        $this->assertModelData($reserva, $createdReserva);
    }

    /**
     * @test read
     */
    public function test_read_reserva()
    {
        $reserva = factory(Reserva::class)->create();

        $dbReserva = $this->reservaRepo->find($reserva->id);

        $dbReserva = $dbReserva->toArray();
        $this->assertModelData($reserva->toArray(), $dbReserva);
    }

    /**
     * @test update
     */
    public function test_update_reserva()
    {
        $reserva = factory(Reserva::class)->create();
        $fakeReserva = factory(Reserva::class)->make()->toArray();

        $updatedReserva = $this->reservaRepo->update($fakeReserva, $reserva->id);

        $this->assertModelData($fakeReserva, $updatedReserva->toArray());
        $dbReserva = $this->reservaRepo->find($reserva->id);
        $this->assertModelData($fakeReserva, $dbReserva->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_reserva()
    {
        $reserva = factory(Reserva::class)->create();

        $resp = $this->reservaRepo->delete($reserva->id);

        $this->assertTrue($resp);
        $this->assertNull(Reserva::find($reserva->id), 'Reserva should not exist in DB');
    }
}
