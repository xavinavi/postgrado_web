<?php namespace Tests\Repositories;

use App\Models\Periodo;
use App\Repositories\PeriodoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PeriodoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PeriodoRepository
     */
    protected $periodoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->periodoRepo = \App::make(PeriodoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_periodo()
    {
        $periodo = factory(Periodo::class)->make()->toArray();

        $createdPeriodo = $this->periodoRepo->create($periodo);

        $createdPeriodo = $createdPeriodo->toArray();
        $this->assertArrayHasKey('id', $createdPeriodo);
        $this->assertNotNull($createdPeriodo['id'], 'Created Periodo must have id specified');
        $this->assertNotNull(Periodo::find($createdPeriodo['id']), 'Periodo with given id must be in DB');
        $this->assertModelData($periodo, $createdPeriodo);
    }

    /**
     * @test read
     */
    public function test_read_periodo()
    {
        $periodo = factory(Periodo::class)->create();

        $dbPeriodo = $this->periodoRepo->find($periodo->id);

        $dbPeriodo = $dbPeriodo->toArray();
        $this->assertModelData($periodo->toArray(), $dbPeriodo);
    }

    /**
     * @test update
     */
    public function test_update_periodo()
    {
        $periodo = factory(Periodo::class)->create();
        $fakePeriodo = factory(Periodo::class)->make()->toArray();

        $updatedPeriodo = $this->periodoRepo->update($fakePeriodo, $periodo->id);

        $this->assertModelData($fakePeriodo, $updatedPeriodo->toArray());
        $dbPeriodo = $this->periodoRepo->find($periodo->id);
        $this->assertModelData($fakePeriodo, $dbPeriodo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_periodo()
    {
        $periodo = factory(Periodo::class)->create();

        $resp = $this->periodoRepo->delete($periodo->id);

        $this->assertTrue($resp);
        $this->assertNull(Periodo::find($periodo->id), 'Periodo should not exist in DB');
    }
}
