<?php namespace Tests\Repositories;

use App\Models\Inscripcion;
use App\Repositories\InscripcionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class InscripcionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var InscripcionRepository
     */
    protected $inscripcionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->inscripcionRepo = \App::make(InscripcionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->make()->toArray();

        $createdInscripcion = $this->inscripcionRepo->create($inscripcion);

        $createdInscripcion = $createdInscripcion->toArray();
        $this->assertArrayHasKey('id', $createdInscripcion);
        $this->assertNotNull($createdInscripcion['id'], 'Created Inscripcion must have id specified');
        $this->assertNotNull(Inscripcion::find($createdInscripcion['id']), 'Inscripcion with given id must be in DB');
        $this->assertModelData($inscripcion, $createdInscripcion);
    }

    /**
     * @test read
     */
    public function test_read_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->create();

        $dbInscripcion = $this->inscripcionRepo->find($inscripcion->id);

        $dbInscripcion = $dbInscripcion->toArray();
        $this->assertModelData($inscripcion->toArray(), $dbInscripcion);
    }

    /**
     * @test update
     */
    public function test_update_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->create();
        $fakeInscripcion = factory(Inscripcion::class)->make()->toArray();

        $updatedInscripcion = $this->inscripcionRepo->update($fakeInscripcion, $inscripcion->id);

        $this->assertModelData($fakeInscripcion, $updatedInscripcion->toArray());
        $dbInscripcion = $this->inscripcionRepo->find($inscripcion->id);
        $this->assertModelData($fakeInscripcion, $dbInscripcion->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_inscripcion()
    {
        $inscripcion = factory(Inscripcion::class)->create();

        $resp = $this->inscripcionRepo->delete($inscripcion->id);

        $this->assertTrue($resp);
        $this->assertNull(Inscripcion::find($inscripcion->id), 'Inscripcion should not exist in DB');
    }
}
