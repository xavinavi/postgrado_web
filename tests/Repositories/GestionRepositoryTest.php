<?php namespace Tests\Repositories;

use App\Models\Gestion;
use App\Repositories\GestionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GestionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GestionRepository
     */
    protected $gestionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->gestionRepo = \App::make(GestionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_gestion()
    {
        $gestion = factory(Gestion::class)->make()->toArray();

        $createdGestion = $this->gestionRepo->create($gestion);

        $createdGestion = $createdGestion->toArray();
        $this->assertArrayHasKey('id', $createdGestion);
        $this->assertNotNull($createdGestion['id'], 'Created Gestion must have id specified');
        $this->assertNotNull(Gestion::find($createdGestion['id']), 'Gestion with given id must be in DB');
        $this->assertModelData($gestion, $createdGestion);
    }

    /**
     * @test read
     */
    public function test_read_gestion()
    {
        $gestion = factory(Gestion::class)->create();

        $dbGestion = $this->gestionRepo->find($gestion->id);

        $dbGestion = $dbGestion->toArray();
        $this->assertModelData($gestion->toArray(), $dbGestion);
    }

    /**
     * @test update
     */
    public function test_update_gestion()
    {
        $gestion = factory(Gestion::class)->create();
        $fakeGestion = factory(Gestion::class)->make()->toArray();

        $updatedGestion = $this->gestionRepo->update($fakeGestion, $gestion->id);

        $this->assertModelData($fakeGestion, $updatedGestion->toArray());
        $dbGestion = $this->gestionRepo->find($gestion->id);
        $this->assertModelData($fakeGestion, $dbGestion->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_gestion()
    {
        $gestion = factory(Gestion::class)->create();

        $resp = $this->gestionRepo->delete($gestion->id);

        $this->assertTrue($resp);
        $this->assertNull(Gestion::find($gestion->id), 'Gestion should not exist in DB');
    }
}
