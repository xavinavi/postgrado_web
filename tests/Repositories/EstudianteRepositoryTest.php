<?php namespace Tests\Repositories;

use App\Models\Estudiante;
use App\Repositories\EstudianteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EstudianteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EstudianteRepository
     */
    protected $estudianteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->estudianteRepo = \App::make(EstudianteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_estudiante()
    {
        $estudiante = factory(Estudiante::class)->make()->toArray();

        $createdEstudiante = $this->estudianteRepo->create($estudiante);

        $createdEstudiante = $createdEstudiante->toArray();
        $this->assertArrayHasKey('id', $createdEstudiante);
        $this->assertNotNull($createdEstudiante['id'], 'Created Estudiante must have id specified');
        $this->assertNotNull(Estudiante::find($createdEstudiante['id']), 'Estudiante with given id must be in DB');
        $this->assertModelData($estudiante, $createdEstudiante);
    }

    /**
     * @test read
     */
    public function test_read_estudiante()
    {
        $estudiante = factory(Estudiante::class)->create();

        $dbEstudiante = $this->estudianteRepo->find($estudiante->id);

        $dbEstudiante = $dbEstudiante->toArray();
        $this->assertModelData($estudiante->toArray(), $dbEstudiante);
    }

    /**
     * @test update
     */
    public function test_update_estudiante()
    {
        $estudiante = factory(Estudiante::class)->create();
        $fakeEstudiante = factory(Estudiante::class)->make()->toArray();

        $updatedEstudiante = $this->estudianteRepo->update($fakeEstudiante, $estudiante->id);

        $this->assertModelData($fakeEstudiante, $updatedEstudiante->toArray());
        $dbEstudiante = $this->estudianteRepo->find($estudiante->id);
        $this->assertModelData($fakeEstudiante, $dbEstudiante->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_estudiante()
    {
        $estudiante = factory(Estudiante::class)->create();

        $resp = $this->estudianteRepo->delete($estudiante->id);

        $this->assertTrue($resp);
        $this->assertNull(Estudiante::find($estudiante->id), 'Estudiante should not exist in DB');
    }
}
