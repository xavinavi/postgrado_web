<?php namespace Tests\Repositories;

use App\Models\Administrativo;
use App\Repositories\AdministrativoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AdministrativoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AdministrativoRepository
     */
    protected $administrativoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->administrativoRepo = \App::make(AdministrativoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_administrativo()
    {
        $administrativo = factory(Administrativo::class)->make()->toArray();

        $createdAdministrativo = $this->administrativoRepo->create($administrativo);

        $createdAdministrativo = $createdAdministrativo->toArray();
        $this->assertArrayHasKey('id', $createdAdministrativo);
        $this->assertNotNull($createdAdministrativo['id'], 'Created Administrativo must have id specified');
        $this->assertNotNull(Administrativo::find($createdAdministrativo['id']), 'Administrativo with given id must be in DB');
        $this->assertModelData($administrativo, $createdAdministrativo);
    }

    /**
     * @test read
     */
    public function test_read_administrativo()
    {
        $administrativo = factory(Administrativo::class)->create();

        $dbAdministrativo = $this->administrativoRepo->find($administrativo->id);

        $dbAdministrativo = $dbAdministrativo->toArray();
        $this->assertModelData($administrativo->toArray(), $dbAdministrativo);
    }

    /**
     * @test update
     */
    public function test_update_administrativo()
    {
        $administrativo = factory(Administrativo::class)->create();
        $fakeAdministrativo = factory(Administrativo::class)->make()->toArray();

        $updatedAdministrativo = $this->administrativoRepo->update($fakeAdministrativo, $administrativo->id);

        $this->assertModelData($fakeAdministrativo, $updatedAdministrativo->toArray());
        $dbAdministrativo = $this->administrativoRepo->find($administrativo->id);
        $this->assertModelData($fakeAdministrativo, $dbAdministrativo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_administrativo()
    {
        $administrativo = factory(Administrativo::class)->create();

        $resp = $this->administrativoRepo->delete($administrativo->id);

        $this->assertTrue($resp);
        $this->assertNull(Administrativo::find($administrativo->id), 'Administrativo should not exist in DB');
    }
}
