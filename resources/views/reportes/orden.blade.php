<!DOCTYPE html>
<html>
<head>
    <title>Nota de Orden</title>
    <style type="text/css">
        body{
            font-size: 16px;
            font-family: "Arial";
        }
        table{
            border-collapse: collapse;
        }
        td{
            padding: 6px 5px;
            font-size: 15px;
        }
        .h1{
            font-size: 21px;
            font-weight: bold;
        }
        .h2{
            font-size: 18px;
            font-weight: bold;
        }
        .tabla1{
            margin-bottom: 20px;
        }
        .tabla2 {
            margin-bottom: 20px;
        }
        .tabla3{
            margin-top: 15px;
        }
        .tabla3 td{
            border: 1px solid #000;
        }
        .tabla3 .cancelado{
            border-left: 0;
            border-right: 0;
            border-bottom: 0;
            border-top: 1px dotted #000;
            width: 200px;
        }
        .emisor{
            color: red;
        }
        .linea{
            border-bottom: 1px dotted #000;
        }
        .border{
            border: 1px solid #000;
        }
        .fondo{
            background-color: #dfdfdf;
        }
        .fisico{
            color: #fff;
        }
        .fisico td{
            color: #fff;
        }
        .fisico .border{
            border: 1px solid #fff;
        }
        .fisico .tabla3 td{
            border: 1px solid #fff;
        }
        .fisico .linea{
            border-bottom: 1px dotted #fff;
        }
        .fisico .emisor{
            color: #fff;
        }
        .fisico .tabla3 .cancelado{
            border-top: 1px dotted #fff;
        }
        .fisico .text{
            color: #000;
        }
        .fisico .fondo{
            background-color: #fff;
        }

    #logo{
            display: none;
        }
    </style>
</head>
<body>
<div class="">
    <table width="100%" class="tabla1">
        <tr>
            <td width="73%" align="center"><h1>Laboratorio San Patricio</h1></td>

            <td width="27%" rowspan="3" align="center" style="padding-right:0">
                <table width="100%">
                    <tr>
                        <td height="50" align="center" class="border"><span class="h2">Nota de Orden</span></td>
                    </tr>
                    {{$orden}}
                     <tr>
                        <td height="50" align="center" class="border"><h2>{{$orden->id}}</h2></td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <table width="100%" class="tabla2">
        <tr>
            <td width="11%">Paciente:</td>
            <td width="37%" class="linea"><span class="text">{{$orden->paciente->nombre}} {{$orden->paciente->apellido_paterno}} {{$orden->paciente->apellido_materno}}</span></td>
            <td width="5%">&nbsp;</td>
            <td width="13%">&nbsp;</td>
            <td width="4%">&nbsp;</td>
            <td width="22%" align="center" class="border fondo"><strong>FECHA</strong></td>

        </tr>
        <tr>
            <td>Telefono:</td>
            <td class="linea"><span class="text">{{$orden->paciente->telefono}}</span></td>
            <td>Edad</td>
            <td class="linea"><span class="text">{{$edad}} Años </span></td>
            <td>&nbsp;</td>
            <td align="center" class="border"><span class="text">{{$orden->created_at}} </span></td>

        </tr>
    </table>
    <table width="100%" class="tabla3">
        <tr>
            <td align="center" class="fondo"><strong>CODIGO</strong></td>
            <td align="center" class="fondo"><strong>ANALISIS</strong></td>
            <td align="center" class="fondo"><strong>TIPO ANALISIS</strong></td>
            <td align="center" class="fondo"><strong>DURACION</strong></td>
        </tr>
        @foreach($orden->analisis as $analisis)

                <tr>
                    <td width="7%" align="center"><span class="text"> {{$analisis->id}}</span></td>
                    <td width="59%"><span class="text">{{$analisis->descripcion}}</span></td>
                    <td width="16%" align="right"><span class="text">{{   DB::table('tipoAnalisis')->where('id', '=', $analisis->tipoAnalisis_id)->first()->nombre}}</span></td>
                    <td width="18%" align="right"><span class="text">{{$analisis->duracion}}</span></td>
                </tr>

        @endforeach


            <td style="border:0;">&nbsp;</td>
            <td align="center" style="border:0;"></td>
            <td style="border:0;">&nbsp;</td>
            <td align="center" style="border:0;" class="emisor"></td>
        </tr>
    </table>

    <table width="100%" class="tabla2">
        <tr>
            <td width="5%">Usuario:</td>
            <td width="37%" class="linea"><span class="text">{{Auth::user()->name}}</span></td>
            <td width="5%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
            <td width="4%">&nbsp;</td>

        </tr>

    </table>

</div>
</body>
</html>