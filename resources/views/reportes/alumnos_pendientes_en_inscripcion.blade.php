@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">alumnos pendiente de inscripcion</h1>
        <br>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <a href="{!! route('reporte.PDF_alumnos_pendientes_en_inscripcion') !!}" class='btn btn-primary '> DESCARGAR<i class="glyphicon glyphicon-file"></i></a>
        <br><br>


        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table" id="programas-table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>apellido paterno</th>
                            <th>apellido materno</th>
                            <th>registro</th>
                            <th>estado </th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($datos as $dato)
                            <tr>
                                <td>{{ $dato->nombre }}</td>
                                <td>{{ $dato->apellido_paterno }}</td>
                                <td>{{ $dato->apellido_materno }}</td>
                                <td>{{ $dato->registro }}</td>
                                <td>{{ $dato->estado }}</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>



            </div>
        </div>
        <div class="text-center">

            @include('adminlte-templates::common.paginate', ['records' => $datos])

        </div>
    </div>
@endsection
