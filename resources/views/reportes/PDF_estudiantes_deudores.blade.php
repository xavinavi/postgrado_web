<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            width: 100%;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 15px;
            text-align: left;
        }

        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }

        table#t01 tr:nth-child(odd) {
            background-color: #fff;
        }

        table#t01 th {
            background-color: black;
            color: white;
        }
    </style>
</head>
<body>

<h2 style="text-align: center">Cantidad de Alumnos  Deudores</h2>
<br>   <table class="table" id="programas-table">
    <thead>
    <tr>
        <th>Nombre del Modulo</th>
        <th>apellido paterno</th>
        <th>apellido materno</th>
        <th>saldo</th>
        <th>monto pagado </th>

    </tr>
    </thead>
    <tbody>
    @foreach($datos as $dato)
        <tr>
            <td>{{ $dato->nombre }}</td>
            <td>{{ $dato->apellido_paterno }}</td>
            <td>{{ $dato->apellido_materno }}</td>
            <td>{{ $dato->saldo }} Bs</td>
            <td>{{ $dato->monto_pagado }} Bs</td>


        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
