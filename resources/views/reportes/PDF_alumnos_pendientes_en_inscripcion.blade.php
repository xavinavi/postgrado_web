<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            width:100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
            text-align: left;
        }
        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }
        table#t01 tr:nth-child(odd) {
            background-color: #fff;
        }
        table#t01 th {
            background-color: black;
            color: white;
        }
    </style>
</head>
<body>

<h2 style="text-align: center">Reporte Alumnos Pendientes de Inscripcion</h2>
<br>

<table id="t01">
    <tr>
        <th>Nombre</th>
        <th>apellido paterno</th>
        <th>apellido materno</th>
        <th>registro</th>
        <th>estado</th>
    </tr>
    @foreach($datos as $dato)
    <tr>
        <td>  {{ $dato->nombre }}</td>
        <td>  {{ $dato->apellido_paterno}}</td>
        <td>  {{ $dato->apellido_materno}}</td>
        <td>  {{ $dato->registro}}</td>
        <td>  {{ $dato->estado}}</td>

    </tr>
@endforeach
</table>

</body>
</html>
