@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Estudiantes deudores</h1>
        <br>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <a href="{!! route('reporte.PDF_estudiantes_deudores') !!}" class='btn btn-primary '> DESCARGAR<i class="glyphicon glyphicon-file"></i></a>
        <br><br>


        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table" id="programas-table">
                        <thead>
                        <tr>
                            <th>Nombre del Modulo</th>
                            <th>apellido paterno</th>
                            <th>apellido materno</th>
                            <th>saldo</th>
                            <th>monto pagado </th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($datos as $dato)
                            <tr>
                                <td>{{ $dato->nombre }}</td>
                                <td>{{ $dato->apellido_paterno }}</td>
                                <td>{{ $dato->apellido_materno }}</td>
                                <td>{{ $dato->saldo }} Bs</td>
                                <td>{{ $dato->monto_pagado }} Bs</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>



            </div>
        </div>
        <div class="text-center">

            @include('adminlte-templates::common.paginate', ['records' => $datos])

        </div>
    </div>
@endsection
