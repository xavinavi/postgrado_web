<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            width: 100%;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 15px;
            text-align: left;
        }

        table#t01 tr:nth-child(even) {
            background-color: #eee;
        }

        table#t01 tr:nth-child(odd) {
            background-color: #fff;
        }

        table#t01 th {
            background-color: black;
            color: white;
        }
    </style>
</head>
<body>

<h2 style="text-align: center">Cantidad de Alumnos por Modulos</h2>
<br>
<table class="table" id="programas-table">
    <thead>
    <tr>
        <th>Nombre del Modulo</th>
        <th>Cantidad de alumnos</th>
    </tr>
    </thead>
    <tbody>
    @foreach($datos as $dato)
        <tr>
            <td>{{ $dato->nombre }}</td>
            <td>{{ $dato->count }}</td>
        </tr>
@endforeach
    </tbody>
</table>
</body>
</html>
