<div class="table-responsive">
    <table class="table" id="profesors-table">
        <thead>
        <tr>
            <th>Ci</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Telefono</th>
            <th>Direccion</th>
            <th>Genero</th>
            <th>Profesion</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($profesors as $profesor)
            <tr>
                <td>{{ $profesor->ci }}</td>
                <td>{{ $profesor->nombre }}</td>
                <td>{{ $profesor->apellido_paterno }}</td>
                <td>{{ $profesor->apellido_materno }}</td>
                <td>{{ $profesor->telefono }}</td>
                <td>{{ $profesor->direccion }}</td>
                <td>{{ $profesor->genero }}</td>
                <td>{{ $profesor->profesion }}</td>
                <td>
                    {!! Form::open(['route' => ['profesors.destroy', $profesor->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('profesors.show')

                            <a href="{{ route('profesors.show', [$profesor->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('profesors.edit')
                            <a href="{{ route('profesors.edit', [$profesor->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('profesors.destroy')

                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
