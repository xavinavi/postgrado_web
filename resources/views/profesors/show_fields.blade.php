
<div class="form-group col-sm-12">


    <h4>Datos Del Estudiante</h4>
    <ul class="list-group">

        <li class="list-group-item"><b>Carnet de Identidad: </b> {{$profesor->Persona->ci}}  </li>
        <li class="list-group-item"><b>Nombre: </b> {{ $profesor->Persona->nombre }} {{ $profesor->Persona->apellido_paterno }} {{ $profesor->Persona->apellido_materno }}</li>
        <li class="list-group-item"><b>Apellido Paterno: </b> {{ $profesor->Persona->apellido_paterno }} </li>
        <li class="list-group-item"><b>Apellido Materno: </b>  {{ $profesor->Persona->apellido_materno }}</li>
        <li class="list-group-item"><b>Telefono: </b>  {{ $profesor->Persona->telefono }}</li>
        <li class="list-group-item"><b>Direccion: </b> {{ $profesor->Persona->direccion }}</li>
        <li class="list-group-item"><b>Genero: </b>{{ $profesor->Persona->genero }}</li>
        <li class="list-group-item"><b>Profecion: </b>{{ $profesor->profesion }}</li>
    </ul>

</div>

