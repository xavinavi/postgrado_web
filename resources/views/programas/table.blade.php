<div class="table-responsive">
    <table class="table" id="programas-table">
        <thead>
        <tr>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>Duracion</th>
            <th>Cupo Minimo</th>
            <th>Descripcion</th>
            <th>Periodo</th>
            <th>Categoria</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($programas as $programa)
            <tr>
                <td>{{ $programa->id }}</td>
                <td>  <a href="{{ route('programa_modulo', [$programa->id]) }}"
                 class='btn btn-primary btn-xs'>
                 {{ $programa->nombre }}
                                    </td>
                <td>{{ $programa->duracion }}</td>
                <td>{{ $programa->cupo_minimo }}</td>
                <td>{{ $programa->descripcion }}</td>
                <td>{{ $programa->periodo_id }} - {{$programa->Periodo->descipcion }}</td>
                <td>{{ $programa->categoria_id }} - {{$programa->Categoria->nombre }}</td>
                <td>
                    {!! Form::open(['route' => ['programas.destroy', $programa->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('programas.show')
                            <a href="{{ route('programas.show', [$programa->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('programas.edit')
                            <a href="{{ route('programas.edit', [$programa->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('programas.destroy')
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
