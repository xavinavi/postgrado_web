<!-- Nombre Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Duracion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duracion', 'Duracion:') !!}
    {!! Form::text('duracion', null, ['class' => 'form-control']) !!}
</div>

<!-- Cupo Minimo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cupo_minimo', 'Cupo Minimo:') !!}
    {!! Form::number('cupo_minimo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Periodo Id Field -->


<div class="form-group col-sm-6">
    <div class="form-group">
        <label for="form_lastname">Seleciona el periodo *</label>
        <select class="form-control select2-simple" name="periodo_id" style="width: 100%;">
            @foreach($periodos  as $data)
                <option value="{{$data->id}}"  @if(!(Request::is('*programas/create')) && ($programa->periodo_id==$data->id))
                selected="selected"
                    @endif>
                    {{$data->id}}  - {{ $data->descipcion }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<!-- Categoria Id Field -->

<div class="form-group col-sm-6">
    <div class="form-group">
        <label for="form_lastname">Seleciona la Categorias *</label>
        <select class="form-control select2-simple" name="categoria_id" style="width: 100%;">
            @foreach($categorias  as $data)
                <option value="{{$data->id}}" @if(!(Request::is('*programas/create')) && ($programa->categoria_id==$data->id))
                selected="selected"
                    @endif>
                    {{$data->id}}  - {{ $data->nombre }}
                </option>
            @endforeach
        </select>
    </div>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('programas.index') }}" class="btn btn-default">Cancel</a>
</div>
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(".select2").select2({
            tags: true
        });
    </script>
@endsection
