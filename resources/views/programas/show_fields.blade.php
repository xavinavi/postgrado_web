<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $programa->id }}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $programa->nombre }}</p>
</div>

<!-- Duracion Field -->
<div class="form-group">
    {!! Form::label('duracion', 'Duracion:') !!}
    <p>{{ $programa->duracion }}</p>
</div>

<!-- Cupo Minimo Field -->
<div class="form-group">
    {!! Form::label('cupo_minimo', 'Cupo Minimo:') !!}
    <p>{{ $programa->cupo_minimo }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $programa->descripcion }}</p>
</div>

<!-- Periodo Id Field -->
<div class="form-group">
    {!! Form::label('periodo_id', 'Periodo Id:') !!}
    <p>{{ $programa->periodo_id }}</p>
</div>

<!-- Categoria Id Field -->
<div class="form-group">
    {!! Form::label('categoria_id', 'Categoria Id:') !!}
    <p>{{ $programa->categoria_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $programa->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $programa->updated_at }}</p>
</div>

