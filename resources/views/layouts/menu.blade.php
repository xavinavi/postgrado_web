@inject('request', 'Illuminate\Http\Request')

<li class="treeview {{ Request::is('administradorUsuario*') ? 'active ' : '' }}">
    <a href="#">
        <i class="fa  fa-user-md"></i> <span>Usuario</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu ">

        @can('users.index')
            <li class="{{ Request::is('*users*') ? 'active ' : '' }}">
                <a href="{!! route('users.index') !!}"><i class="fa fa-circle-o"></i><span>Gestionar Usuario</span></a>
            </li>
        @endcan
        @can('roles.index')

            <li class="{{ Request::is('*roles*') ? 'active ' : '' }}">
                <a href="{!! route('roles.index') !!}"><i class="fa fa-circle-o"></i><span>Gestionar Roles</span></a>
            </li>
        @endcan

        @can('roles.index')
            <li class="{{ Request::is('profesors*') ? 'active' : '' }}">
                <a href="{{ route('profesors.index') }}"><i class="fa fa-edit"></i><span>Profesors</span></a>
            </li>
        @endcan

        @can('roles.index')
            <li class="{{ Request::is('estudiantes*') ? 'active' : '' }}">
                <a href="{{ route('estudiantes.index') }}"><i class="fa fa-edit"></i><span>Estudiantes</span></a>
            </li>
        @endcan
        @can('administrativos.index')
            <li class="{{ Request::is('administrativos*') ? 'active' : '' }}">
                <a href="{{ route('administrativos.index') }}"><i
                        class="fa fa-edit"></i><span>Administrativos</span></a>
            </li>
        @endcan
    </ul>
</li>


<li class="treeview {{ Request::is('programas*') ? 'active ' : '' }}">
    <a href="#">
        <i class="fa  fa-medkit"></i> <span>Programas</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu ">
        @can('programas.index')
            <li class="{{ Request::is('programas/programas*') ? 'active' : '' }}">
                <a href="{{ route('programas.index') }}"><i class="fa fa-edit"></i><span>Programas</span></a>
            </li>
        @endcan
        @can('modulos.index')

            <li class="{{ Request::is('programas/modulos*') ? 'active' : '' }}">
                <a href="{{ route('modulos.index') }}"><i class="fa fa-edit"></i><span>Modulos</span></a>
            </li>
        @endcan

        @can('ofertas.index')

            <li class="{{ Request::is('programas/ofertas*') ? 'active' : '' }}">
                <a href="{{ route('ofertas.index') }}"><i class="fa fa-edit"></i><span>Ofertas</span></a>
            </li>
        @endcan
        @can('gestions.index')

            <li class="{{ Request::is('programas/gestions*') ? 'active' : '' }}">
                <a href="{{ route('gestions.index') }}"><i class="fa fa-edit"></i><span>Gestions</span></a>
            </li>
        @endcan
        @can('periodos.index')

            <li class="{{ Request::is('programas/periodos*') ? 'active' : '' }}">
                <a href="{{ route('periodos.index') }}"><i class="fa fa-edit"></i><span>Periodos</span></a>
            </li>
        @endcan
        @can('categorias.index')

            <li class="{{ Request::is('programas/categorias*') ? 'active' : '' }}">
                <a href="{{ route('categorias.index') }}"><i class="fa fa-edit"></i><span>Categorias</span></a>
            </li>
        @endcan

    </ul>
</li>


<li class="treeview {{ Request::is('reservaInscripcion*') ? 'active ' : '' }}">
    <a href="#">
        <i class="fa  fa-medkit"></i> <span>Reserva e Inscripcion</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu ">
        @can('categorias.index')

        <li class="{{ Request::is('reservaInscripcion/reservas*') ? 'active' : '' }}">
            <a href="{{ route('reservas.index') }}"><i class="fa fa-edit"></i><span>Reservas</span></a>
        </li>
        @endcan
            @can('inscripcions.index')

        <li class="{{ Request::is('reservaInscripcion/inscripcions*') ? 'active' : '' }}">
            <a href="{{ route('inscripcions.index') }}"><i class="fa fa-edit"></i><span>Inscripcions</span></a>
        </li>
            @endcan
            @can('pagos.index')

        <li class="{{ Request::is('reservaInscripcion/pagos*') ? 'active' : '' }}">
            <a href="{{ route('pagos.index') }}"><i class="fa fa-edit"></i><span>Pagos</span></a>
        </li>
            @endcan

    </ul>
</li>


<li class="treeview {{ Request::is('reportes*') ? 'active ' : '' }}">
    <a href="#">
        <i class="fa  fa-medkit"></i> <span>Reportes</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu ">

        <li class="{{ Request::is('reportes/alumnos_pendientes_en_inscripcion*') ? 'active' : '' }}">
            <a href="{{ route('reporte.alumnos_pendientes_en_inscripcion') }}"><i class="fa fa-edit"></i><span>pendiente de inscripcion</span></a>
        </li>

        <li class="{{ Request::is('reportes/alumnos_reprobados*') ? 'active' : '' }}">
            <a href="{{ route('reporte.alumnos_reprobados') }}"><i class="fa fa-edit"></i><span>reprovados</span></a>
        </li>
        <li class="{{ Request::is('reportes/estudiantes_deudores*') ? 'active' : '' }}">
            <a href="{{ route('reporte.estudiantes_deudores') }}"><i class="fa fa-edit"></i><span>deudores</span></a>
        </li>
        <li class="{{ Request::is('reportes/lista_profesores_materia*') ? 'active' : '' }}">
            <a href="{{ route('reporte.lista_profesores_materia') }}"><i class="fa fa-edit"></i><span>profesores por materia</span></a>
        </li>
        <li class="{{ Request::is('reportes/recaudacion_por_modulo*') ? 'active' : '' }}">
            <a href="{{ route('reporte.recaudacion_por_modulo') }}"><i class="fa fa-edit"></i><span>recaudacion por modulo</span></a>
        </li>

        <li class="{{ Request::is('reportes/cantidad_alumnos_modulo*') ? 'active' : '' }}">
            <a href="{{ route('reporte.cantidad_alumnos_modulo') }}"><i
                    class="fa fa-edit"></i><span>cantidad de Estu..</span></a>
        </li>

        <li class="{{ Request::is('reportes/reporte_bi*') ? 'active' : '' }}">
            <a href="{{ route('reporte.reporte_bi') }}"><i class="fa fa-edit"></i><span>Reporte BI</span></a>
        </li>


    </ul>
</li>


{{-----
<li class="{{ Request::is('agendas*') ? 'active' : '' }}">

    <a href="{!! route('agendas.index') !!}"><i class="fa fa-calendar"></i><span>Agendas</span></a>
</li>


----}}


















