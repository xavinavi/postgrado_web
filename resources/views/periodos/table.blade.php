<div class="table-responsive">
    <table class="table" id="periodos-table">
        <thead>
        <tr>
            <th>Fecha Inicio</th>
            <th>Fecha Fin</th>
            <th>Descipcion</th>
            <th>Gestion</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($periodos as $periodo)
            <tr>
                <td>{{ $periodo->fecha_inicio->format('Y-m-d') }}</td>
                <td>{{ $periodo->fecha_fin->format('Y-m-d') }}</td>
                <td>{{ $periodo->descipcion }}</td>
                <td> {{ $periodo->gestions->año }}</td>
                <td>
                    {!! Form::open(['route' => ['periodos.destroy', $periodo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('periodos.show')
                            <a href="{{ route('periodos.show', [$periodo->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('periodos.edit')
                            <a href="{{ route('periodos.edit', [$periodo->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('periodos.destroy')
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
