<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $periodo->id }}</p>
</div>

<!-- Feha Inicio Field -->
<div class="form-group">
    {!! Form::label('fecha_inicio', 'Feha Inicio:') !!}
    <p>{{ $periodo->fecha_inicio }}</p>
</div>

<!-- Feha Fin Field -->
<div class="form-group">
    {!! Form::label('fecha_fin', 'Feha Fin:') !!}
    <p>{{ $periodo->fecha_fin }}</p>
</div>

<!-- Descipcion Field -->
<div class="form-group">
    {!! Form::label('descipcion', 'Descipcion:') !!}
    <p>{{ $periodo->descipcion }}</p>
</div>

<!-- Gestion Id Field -->
<div class="form-group">
    {!! Form::label('gestion_id', 'Gestion Id:') !!}
    <p>{{ $periodo->gestion_id }}</p>
</div>

