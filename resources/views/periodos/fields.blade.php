<!-- Feha Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_inicio', 'Feha Inicio:') !!}
    {!! Form::date('fecha_inicio', Request::is('*periodos/create') ? null : $periodo->fecha_inicio, ['class' => 'form-control','id'=>'fecha_inicio']) !!}
</div>




<!-- Feha Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_fin', 'Feha Fin:') !!}
    {!! Form::date('fecha_fin', Request::is('*periodos/create') ? null : $periodo->fecha_fin, ['class' => 'form-control','id'=>'fecha_fin']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_fin').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#fecha_inicio').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<!-- Descipcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descipcion', 'Descipcion:') !!}
    {!! Form::textarea('descipcion', null, ['class' => 'form-control']) !!}
</div>


<div class="col-lg-12">
    <div class="form-group">
        <label for="form_lastname">Seleciona el Periodo *</label>
        <select class="form-control select2-simple" name="gestion_id" style="width: 100%;">
            @foreach($gestion  as $dato)
                <option value="{{$dato->id}}"  @if(!(Request::is('*periodos/create')) && ($periodo->gestion_id==$dato->id))
                selected="selected"
                @endif
>
                ({{$dato->id}}) - {{ $dato->año }}
                </option>
            @endforeach
        </select>
    </div>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('periodos.index') }}" class="btn btn-default">Cancel</a>
</div>


