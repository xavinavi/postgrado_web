<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $user->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $user->email !!}</p>
</div>



<!-- Empleado Id Field -->
<div class="form-group">
    {!! Form::label('empleado_id', 'Empleado Id:') !!}
    <p></p>
    <td>
        {{---
        <a href="{!! route('empleados.show', [$user->empleado_id ]) !!}" class='btn btn-primary btn-xs'>({!! $user->empleado_id !!}) ver datos del empleado</a>
  --}}
    </td>
</div>

<!-- Remember Token Field -->


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $user->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $user->updated_at !!}</p>
</div>

<hr>


<h3>Lista de roles</h3>
<div class="form-group">
    <ul class="list-unstyled">
        @foreach($user->roles as $role)
            <li>
                <label>
                    {{ $role->name }}
                    <em>({{ $role->description }})</em>
                </label>
            </li>
        @endforeach
    </ul>
</div>
