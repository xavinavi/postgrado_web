<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('password', 'Confirmar Password:') !!}
    {!! Form::password('confirm-password', ['class' => 'form-control']) !!}
</div>

<div class="col-lg-12">
    <div class="form-group">
        <label for="form_lastname">Seleciona el la Persona *</label>
        <select class="form-control select2" name="empleado_id" >
            @foreach($empleados  as $empleado)
                <option value="{{$empleado->id}}">({{$empleado->id}}) - {{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}</option>
            @endforeach
        </select>
    </div>
</div>


<div class="form-group col-sm-12">
    <hr>
    <h3>Lista de roles</h3>
    <ul class="list-unstyled">
        @foreach($roles as $role)
            <li>
                <label>
                    {{ Form::checkbox('roles[]', $role->id, null) }}
                    {{ $role->name }}
                    <em>({{ $role->description }})</em>
                </label>
            </li>
        @endforeach
    </ul>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
