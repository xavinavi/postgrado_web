@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pago
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('pagos.show_fields')
                    <a href="{{ route('pagos.index') }}" class="btn btn-default">Back</a>
                    @if($pago->saldo>0)
                        <a href="{{ route('pagar.saldo.inscripcion', [$pago->inscripcion_id]) }}" class='btn btn-success'>pagar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
