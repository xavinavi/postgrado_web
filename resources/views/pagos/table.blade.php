<div class="table-responsive">
    <table class="table" id="pagos-table">
        <thead>
        <tr>
            <th>Codigo</th>
            <th>Estudiante</th>
            <th>Fecha</th>
            <th>Monto</th>
            <th>Saldo</th>
            <th>Pago por:</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pagos as $pago)
            <tr>
                <td>{{ $pago->id }}</td>
                <td>
                    {{ $pago->Inscripcion->Estudiante->registro }}
                    {{ $pago->Inscripcion->Estudiante->Persona->nombre }}
                    {{ $pago->Inscripcion->Estudiante->Persona->apellido_paterno }}
                </td>
                <td>{{ $pago->fecha->format('Y-m-d') }}</td>
                <td>{{ $pago->monto }}</td>
                <td>{{ $pago->saldo }}</td>
                <td>{{ $pago->nombre }}</td>
                <td>
                    <div class='btn-group'>
                        @can('pagar.saldo.inscripcion')
                        @if($pago->saldo>0)
                            <a href="{{ route('pagar.saldo.inscripcion', [$pago->inscripcion_id]) }}"
                               class='btn btn-success btn-xs'>pagar</a>
                        @endif
                        @endcan
                            @can('pagos.show')

                            <a href="{{ route('pagos.show', [$pago->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                            @endcan
                    </div>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
