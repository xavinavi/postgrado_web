@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Pago
    </h1>
</section>
<div class="content">
    @include('adminlte-templates::common.errors')
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
            {!! Form::model($inscripcion, ['route' => ['pagar.reserva.post', $inscripcion->id], 'method' => 'put']) !!}
                <div class="form-group col-sm-12">
                    <h4>Datos del Estudiante</h4>

                    <ul class="list-group">
                    <li class="list-group-item"><b>Codigo Estudiante:</b> {{ $inscripcion->Estudiante->registro }} </li>
                    <li class="list-group-item"><b>Carnet de Identidad: </b> {{ $inscripcion->Estudiante->Persona->ci}}  </li>
                    <li class="list-group-item"><b>Nombre: </b> {{ $inscripcion->Estudiante->Persona->nombre }} {{ $inscripcion->Estudiante->Persona->apellido_paterno }} {{ $inscripcion->Estudiante->Persona->apellido_materno }}</li>
                </ul>
                <hr>
                <h4>Modulos</h4>
                <ul class="list-group">
                    @foreach($inscripcion->modulos as $modulo)
                        <li class="list-group-item">
                            <b> {{ $modulo->nombre }} </b>
                            <br>
                            <small> [ fecha inicio: {{ $modulo->fecha_inicio->format('Y-m-d') }} , Fecha fin:   {{ $modulo->fecha_fin->format('Y-m-d') }}]
                            <br> [ Hora Inicio: {{ $modulo->hora_inicio }}, Hora Salida {{ $modulo->hora_salida }}]
                            </small>
                            <hr><b>Precio: {{$modulo->costo}} /Bs.</b>

                        </li>
                    @endforeach
                </ul>
                <p>monto total : {{$inscripcion->modulos->sum('costo')}}</p>

                </div>
                <!-- Monto Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('monto', 'Monto:') !!}
                    {!! Form::text('monto', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Nombre Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('nombre', 'Por concepto de:') !!}
                    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Descripcion Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('descripcion', 'Descripcion:') !!}
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('reservas.index') }}" class="btn btn-default">Cancel</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
