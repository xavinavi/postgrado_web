<!-- Id Field -->

<div class="form-group col-sm-12">


    <h4>Datos Del Pago</h4>

    <ul class="list-group">
        <li class="list-group-item"><b>Codigo del pago:</b> {{ $pago->id }} </li>
        <li class="list-group-item"><b>Fecha: </b> {{ $pago->fecha}}  </li>
    </ul>
    <h4>Datos Del Estudiante</h4>

    <ul class="list-group">
        <li class="list-group-item"><b>Codigo Estudiante:</b> {{ $pago->Inscripcion->Estudiante->registro }} </li>
        <li class="list-group-item"><b>Carnet de Identidad: </b> {{ $pago->Inscripcion->Estudiante->Persona->ci}}  </li>
        <li class="list-group-item"><b>Nombre: </b> {{ $pago->Inscripcion->Estudiante->Persona->nombre }} {{ $pago->Inscripcion->Estudiante->Persona->apellido_paterno }} {{ $pago->Inscripcion->Estudiante->Persona->apellido_materno }}</li>
    </ul>

    <h4>Modulos</h4>
    <ul class="list-group">
        @foreach($pago->Inscripcion->modulos as $modulo)
            <li class="list-group-item">
                <b> {{ $modulo->nombre }}  </b>
                <br>
                <small> [ fecha inicio: {{ $modulo->fecha_inicio->format('Y-m-d') }} , Fecha fin:   {{ $modulo->fecha_fin->format('Y-m-d') }}]
                    <br> [ Hora Inicio: {{ $modulo->hora_inicio }}, Hora Salida {{ $modulo->hora_salida }}]
                </small>
                <hr><b>Precio: {{$modulo->costo}} /Bs.</b>
            </li>
        @endforeach
    </ul>

    <h4>Detalle de los pagos</h4>
    <ul class="list-group">
            <li class="list-group-item"><b> monto total : {{$pago->Inscripcion->modulos->sum('costo')}}</b></li>
            <li class="list-group-item"><b>monto Pagado : {{$pago->Inscripcion->modulos->sum('costo') -  DB::table('pagos')->where('inscripcion_id', $pago->Inscripcion->id)->select('saldo')->first()->saldo}}</b></li>
            <li class="list-group-item"><b> monto por pagar : {{DB::table('pagos')->where('inscripcion_id', $pago->Inscripcion->id)->select('saldo')->first()->saldo}}</b></li>
    </ul>




    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Detalle de Pagos</div>

        <!-- Table -->
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Monto</th>
            </tr>
            </thead>

            <tbody>

            @foreach($pago->inscripciones as $pago)
                <tr>
                    <td>{{ $pago->pivot->fecha }}</td>
                    <td>{{ $pago->pivot->pago }}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>



<!-- Inscripcion Id Field -->
{{----
<div class="form-group">
    {!! Form::label('inscripcion_id', 'Inscripcion Id:') !!}
    <p>{{ $pago->inscripcion_id }}</p>
</div>
---}}


