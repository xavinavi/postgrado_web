@extends('layouts.app')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Pagos</h1>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <br>
        {{ Form::open(['route' => 'pagos.index', 'method' => 'GET', 'class' => 'form-inline ']) }}
        <div class="form-group">
            {{ Form::text('codigo', $codigo, ['class' => 'form-control', 'placeholder' => 'Codigo']) }}
        </div>
        <div class="form-group">
            <select class="form-control select2" name="estado" style="width: 100%;">
                <option value="" >todos</option>
                <option value="Por Pagar" @if($estado=='Por Pagar') selected @endif>Por Pagar</option>
                <option value="saldo Cancelado"  @if($estado=='saldo Cancelado') selected @endif>saldo Cancelado</option>
            </select>
        </div>
        <div class="form-group">
            {{ Form::text('fecha', null, ['class' => 'form-control reportrange', 'placeholder' => 'fecha']) }}

            <div class="form-group">
                {{ Form::text('estudiante', $estudiante, ['class' => 'form-control', 'placeholder' => 'Estudiante']) }}
            </div>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>Buscar
            </button>
        </div>
        {{ Form::close() }}
        <br>
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('pagos.table')
            </div>
        </div>
        <div class="text-center">

            @include('adminlte-templates::common.paginate', ['records' => $pagos])

        </div>
    </div>
@endsection


@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
    </script>
    <script type="text/javascript">
        $(function () {

            $('input[name="fecha"]').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
            let start = moment().subtract(29, 'days');
            let end = moment();

            @if($fecha)
            start = @json( $fecha[0]);
            end = @json($fecha[1] );
            @endif

            $('.reportrange').daterangepicker({
                "locale": {
                    "format": "YYYY-MM-DD",
                    "separator": " to ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    // "firstDay": 1
                },
                startDate: start,
                endDate: end,
                alwaysShowCalendars: true,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 últimos días': [moment().subtract(6, 'days'), moment()],
                    '30 últimos días': [moment().subtract(29, 'days'), moment()],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                //autoUpdateInput: false,
                //  autoUpdateInput: false,

            });


            //  cb("", "");

        });
    </script>
@endsection

