<div class="table-responsive">
    <table class="table" id="personas-table">
        <thead>
            <tr>
                <th>Ci</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Telefono</th>
                <th>Direccion</th>
                <th>Genero</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($personas as $persona)
            <tr>
                <td>{{ $persona->ci }}</td>
                <td>{{ $persona->nombre }}</td>
                <td>{{ $persona->apellido_paterno }}</td>
                <td>{{ $persona->apellido_materno }}</td>
                <td>{{ $persona->telefono }}</td>
                <td>{{ $persona->direccion }}</td>
                <td>{{ $persona->genero }}</td>
                <td>
                    {!! Form::open(['route' => ['personas.destroy', $persona->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('personas.show', [$persona->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('personas.edit', [$persona->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
