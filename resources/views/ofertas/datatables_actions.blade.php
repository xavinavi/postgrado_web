{!! Form::open(['route' => ['ofertas.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @can('ofertas.show')
    <a href="{{ route('ofertas.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    @endcan
        @can('ofertas.edit')

        <a href="{{ route('ofertas.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
        @endcan
        @can('ofertas.destroy')
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}

        @endcan
</div>
{!! Form::close() !!}
