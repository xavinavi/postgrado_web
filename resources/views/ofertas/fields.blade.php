<!-- Nombre Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha de Vencimiento de la oferta:') !!}
    {!! Form::date('fecha',  Request::is('*ofertas/create') ? null : $oferta->fecha, ['class' => 'form-control','id'=>'fecha']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<div class="form-group col-sm-6">
    <div class="form-group">
        <label for="form_lastname">Seleciona el el modulo *</label>
        <select class="form-control select2-simple" name="programa_id" style="width: 100%;">
            @foreach($programas  as $data)
                <option value="{{$data->id}}"
                        @if(!(Request::is('*ofertas/create')) && ($oferta->programa_id==$data->id))
                        selected="selected"
                    @endif
                >
                    {{$data->id}}  - {{ $data->nombre }}
                </option>
            @endforeach
        </select>
    </div>
</div>


<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Programa Id Field -->


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ofertas.index') }}" class="btn btn-default">Cancel</a>
</div>
