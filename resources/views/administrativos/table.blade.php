<div class="table-responsive">
    <table class="table" id="administrativos-table">
        <thead>
        <tr>
            <th>Ci</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Telefono</th>
            <th>Direccion</th>
            <th>Genero</th>
            <th>Cargo</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($administrativos as $administrativo)
            <tr>
                <td>{{ $administrativo->ci }}</td>
                <td>{{ $administrativo->nombre }}</td>
                <td>{{ $administrativo->apellido_paterno }}</td>
                <td>{{ $administrativo->apellido_materno }}</td>
                <td>{{ $administrativo->telefono }}</td>
                <td>{{ $administrativo->direccion }}</td>
                <td>{{ $administrativo->genero }}</td>
                <td>{{ $administrativo->cargo }}</td>
                <td>
                    {!! Form::open(['route' => ['administrativos.destroy', $administrativo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('administrativos.show')
                            <a href="{{ route('administrativos.show', [$administrativo->id]) }}"
                               class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('administrativos.edit')
                            <a href="{{ route('administrativos.edit', [$administrativo->id]) }}"
                               class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('administrativos.destroy')
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
