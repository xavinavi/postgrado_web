
<div class="form-group col-sm-12">


    <h4>Datos Del Estudiante</h4>
    <ul class="list-group">

        <li class="list-group-item"><b>Carnet de Identidad: </b> {{$administrativo->Persona->ci}}  </li>
        <li class="list-group-item"><b>Nombre: </b> {{ $administrativo->Persona->nombre }} {{ $administrativo->Persona->apellido_paterno }} {{ $administrativo->Persona->apellido_materno }}</li>
        <li class="list-group-item"><b>Apellido Paterno: </b> {{ $administrativo->Persona->apellido_paterno }} </li>
        <li class="list-group-item"><b>Apellido Materno: </b>  {{ $administrativo->Persona->apellido_materno }}</li>
        <li class="list-group-item"><b>Telefono: </b>  {{ $administrativo->Persona->telefono }}</li>
        <li class="list-group-item"><b>Direccion: </b> {{ $administrativo->Persona->direccion }}</li>
        <li class="list-group-item"><b>Genero: </b>{{ $administrativo->Persona->genero }}</li>
        <li class="list-group-item"><b>Cargo: </b>{{ $administrativo->cargo }}</li>
    </ul>

</div>

