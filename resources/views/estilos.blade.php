@extends('layouts.app')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2 {
            width:100%!important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Estilos</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('estilos.restablecer') !!}">Restablcer</a>
        </h1>
        <br>
    </section>
    <br>
    <div class="content">

        @include('flash::message')
        <div class="row">
            <a href="{{route('estilos.store.templete','skin-blue sidebar-mini')}}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-black"><i class="ion ion-android-menu"></i></span>

                        <div class="info-box-content bg-blue">
                            <span class="info-box-text">AZUL</span>
                            <span class="info-box-text">NEGRO</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <!-- /.col -->
            <a href="{{route('estilos.store.templete','sidebar-mini skin-black')}}">

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="ion ion-android-menu"></i></span>

                    <div class="info-box-content  bg-white">
                        <span class="info-box-text">BLANCO</span>
                        <span class="info-box-text">NEGRO</span>

                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-purple')}}">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="ion ion-android-menu"></i></span>
                    <div class="info-box-content bg-purple">
                        <span class="info-box-text">PURPURA</span>
                        <span class="info-box-text">NEGRO</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-green')}}">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="ion ion-android-menu"></i></span>

                    <div class="info-box-content bg-green">
                        <span class="info-box-text">VERDE</span>
                        <span class="info-box-text">NEGRO</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-red')}}">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="ion ion-android-menu"></i></span>

                    <div class="info-box-content bg-red">
                        <span class="info-box-text">ROJO</span>
                        <span class="info-box-text">NEGRO</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            </a>

          <a href="{{route('estilos.store.templete','sidebar-mini skin-yellow')}}">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="ion ion-android-menu"></i></span>

                    <div class="info-box-content bg-yellow">
                        <span class="info-box-text">AMARILLO</span>
                        <span class="info-box-text">NEGRO</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
          </a>
            <!-- /.col -->

            {{---  Inicio de plantilla con menu blanco---}}


            <div class="clearfix"></div>
            <div class="box box-primary"></div>



            <a href="{{route('estilos.store.templete','sidebar-mini skin-blue-light')}}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="ion ion-android-menu"></i></span>

                        <div class="info-box-content bg-blue">
                            <span class="info-box-text">AZUL</span>
                            <span class="info-box-text">BLANCO</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <!-- /.col -->
            <a href="{{route('estilos.store.templete','sidebar-mini skin-black-light')}}">

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="ion ion-android-menu"></i></span>

                        <div class="info-box-content  bg-white">
                            <span class="info-box-text">BLANCO</span>
                            <span class="info-box-text">BLANCO</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-purple-light')}}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="ion ion-android-menu"></i></span>
                        <div class="info-box-content bg-purple">
                            <span class="info-box-text">PURPURA</span>
                            <span class="info-box-text">BLANCO</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-green-light')}}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="ion ion-android-menu"></i></span>

                        <div class="info-box-content bg-green">
                            <span class="info-box-text">VERDE</span>
                            <span class="info-box-text">BLANCO</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-red-light')}}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="ion ion-android-menu"></i></span>

                        <div class="info-box-content bg-red">
                            <span class="info-box-text">ROJO</span>
                            <span class="info-box-text">BLANCO</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="{{route('estilos.store.templete','sidebar-mini skin-yellow-light')}}">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-white"><i class="ion ion-android-menu"></i></span>

                        <div class="info-box-content bg-yellow">
                            <span class="info-box-text">AMARILLO</span>
                            <span class="info-box-text">BLANCO</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
            {{---  fin de plantilla con menu blanco---}}

        </div>

    </div>



    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    <section class="content-header">
                        <h1>
                            Estilo de letra
                        </h1>
                    </section>
                    <hr>
                    {!! Form::open(['route' => 'estilos.store.font']) !!}
                    <div class="form-group col-sm-6">
                        {!! Form::label('font-family', 'Seleccione El Estilo de Letra *') !!}
                        <select class="select2 col-sm-12" name="font-family" >
                            <option value="Agency FB">Agency FB</option>
                            <option value="Algerian">Algerian</option>
                            <option value="Americana">Americana</option>
                            <option value="Arial">Arial</option>
                            <option value="BakerSignet">BakerSignet</option>
                            <option value="Berlin Sans FB">Berlin Sans FB</option>
                            <option value="Birch">Birch</option>
                            <option value="Book Antiqua">Book Antiqua</option>
                            <option value="Bradley Hand ITC">Bradley Hand ITC</option>
                            <option value="Brush Script MT">Brush Script MT</option>
                            <option value="Castellar">Castellar</option>
                            <option value="Centaur">Centaur</option>
                            <option value="Century Gothic">Century Gothic</option>
                            <option value="Charlemagne">Charlemagne</option>
                            <option value="Chiller">Chiller</option>
                            <option value="Colonna MT">Colonna MT</option>
                            <option value="Comic Sans MS">Comic Sans MS</option>
                            <option value="Courier">Courier</option>
                            <option value="Curlz MT">Curlz MT</option>
                            <option value="Elephant">Elephant</option>
                            <option value="Engravers MT">Engravers MT</option>
                            <option value="Forte">Forte</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Goudy">Goudy</option>
                            <option value="Harrington">Harrington</option>
                            <option value="High Tower Text">High Tower Text</option>
                            <option value="Hobo">Hobo</option>
                            <option value="Impact">Impact</option>
                            <option value="Jazz Poster ICG">Jazz Poster ICG</option>
                            <option value="Book Antiqua">Book Antiqua</option>
                            <option value="Jokerman">Jokerman</option>
                            <option value="Kaufmann">Kaufmann</option>
                            <option value="Lucida Handwriting">Lucida Handwriting</option>
                            <option value="Magneto">Magneto</option>
                            <option value="Matisse ITC">Matisse ITC</option>
                            <option value="Mistral">Mistral</option>
                            <option value="Monotype Corsiva">Monotype Corsiva</option>
                            <option value="News Gothic">News Gothic</option>
                            <option value="Poor Richard">Poor Richard</option>
                            <option value="Ravie">Ravie</option>
                            <option value="Showcard Gothic">Showcard Gothic</option>
                            <option value="Tigerteeth ICG">Tigerteeth ICG</option>
                            <option value="Times New Roman">Times New Roman</option>
                            <option value="Wide Latin">Wide Latin</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('font-size', 'Seleccione El Tamaño de la Letra *') !!}
                        <select class="select2 col-sm-12" name="font-size" >
                            <option value="8pt">8pt</option>
                            <option value="10pt">10pt</option>
                            <option value="11pt">11pt</option>
                            <option value="12pt">12pt</option>
                            <option value="14pt">14pt</option>
                            <option value="16pt">16pt</option>
                            <option value="18pt">18pt</option>
                            <option value="20pt">20pt</option>
                            <option value="24pt">24pt</option>
                            <option value="36pt">36pt</option>
                            <option value="48pt">48pt</option>
                            <option value="72pt">72pt</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-12">
                        <hr>

                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select-multiple').select2();
        });
    </script>
@endsection
