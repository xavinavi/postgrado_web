@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reserva
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($reserva, ['route' => ['reservas.update', $reserva->id], 'method' => 'patch']) !!}

                        @include('reservas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection