@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reserva
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('reservas.show_fields')

                    @if($reserva->estado=="reservado")

                        {!! Form::open(['route' => ['reserva.inscribir', $reserva->id], 'method' => 'put']) !!}
                        <a href="{{ route('reservas.index') }}" class="btn btn-default">Back</a>
                        {!! Form::button('Inscribir', ['type' => 'submit', 'class' => 'btn btn-success ', 'onclick' => "return confirm('Esta Seguro que quiere Inscribir?')"]) !!}
                        {!! Form::close() !!}
                    @else
                        <a href="{{ route('reservas.index') }}" class="btn btn-default">Back</a>

                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
