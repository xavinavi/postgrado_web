<div class="table-responsive">
    <table class="table"  style="width:100%" border=1 id="reservas-table">
        <thead>

        <tr>
            <th>Codigo</th>
            <th>Estado</th>
            <th style="width:90px;">Fecha</th>
            <th>User</th>
            <th>Estudiante</th>
            <th style="width:80px;">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reservas as $reserva)
            <tr>
                <td>{{ $reserva->id }}</td>
                <td>
                    @if($reserva->estado=="reservado")
                        <div class='btn-group'>
                            @can('reservas.create')
                            {!! Form::open(['route' => ['reserva.inscribir', $reserva->id], 'method' => 'put']) !!}
                            {!! Form::button('Inscribir', ['type' => 'submit', 'class' => 'btn btn-success btn-xs', 'onclick' => "return confirm('Esta Seguro que quiere Inscribir?')"]) !!}
                            {!! Form::close() !!}
                            @endcan
                                @can('reservas.create')
                            {!! Form::open(['route' => ['reserva.anular', $reserva->id], 'method' => 'put']) !!}
                            {!! Form::button('Anular&nbsp&nbsp', ['type' => 'submit', 'class' => 'btn btn-warning btn-xs', 'onclick' => "return confirm('Esta Seguro que quiere Inscribir?')"]) !!}
                            {!! Form::close() !!}
                             @endcan
                        </div>
                    @elseif($reserva->estado=="inscrito")

                        <span class="label label-primary">{{ $reserva->estado }}</span>

                    @elseif($reserva->estado=="anulado")
                        <span class="label label-info">{{ $reserva->estado }}</span>

                    @endif
                </td>
                <td>{{ $reserva->fecha->format('Y-m-d') }}</td>
                <td><a href="{{route('users.show',$reserva->user_id)}}">({{ $reserva->user_id }})
                        {{ $reserva->user->persona->nombre }} {{ $reserva->user->persona->apellido_paterno }} {{ $reserva->user->persona->apellido_materno }}
                        </a></td>
                <td>
                    <a href="{{route('estudiantes.show',$reserva->Estudiante->id)}}"> ({{ $reserva->Estudiante->registro }})
                        {{ $reserva->Estudiante->Persona->nombre }} {{ $reserva->Estudiante->persona->apellido_paterno }} {{ $reserva->Estudiante->persona->apellido_materno }}
                    </a>
                </td>

                <td>
                    <!-- Split button -->

                    {!! Form::open(['route' => ['reservas.destroy', $reserva->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('reservas.show')
                        <a href="{{ route('reservas.show', [$reserva->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                        @endif
                            @can('reservas.destroy')
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            @endif
                    </div>
                    {!! Form::close() !!}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
