
<div class="form-group col-sm-12">

    <h4>Datos De la Reserva</h4>

    <ul class="list-group">
        <li class="list-group-item"><b>Codigo:</b> {{ $reserva->id}} </li>
        <li class="list-group-item"><b>Fecha: </b> {{$reserva->fecha}}  </li>
        <li class="list-group-item"><b>Estado: </b> {{$reserva->estado}}  </li>
    </ul>
    <h4>Datos Del Estudiante</h4>
    <ul class="list-group">
        <li class="list-group-item"><b>Codigo Estudiante:</b> {{ $reserva->Estudiante->registro }} </li>
        <li class="list-group-item"><b>Carnet de Identidad: </b> {{ $reserva->Estudiante->Persona->ci}}  </li>
        <li class="list-group-item"><b>Nombre: </b> {{ $reserva->Estudiante->Persona->nombre }} {{ $reserva->Estudiante->Persona->apellido_paterno }} {{ $reserva->Estudiante->Persona->apellido_materno }}</li>
    </ul>



    <h4>Modulos</h4>
    <ul class="list-group">
        @foreach($reserva->modulos as $modulo)
            <li class="list-group-item">
                <b> {{ $modulo->nombre }}  </b>
                <br>
                <small> [ fecha inicio: {{ $modulo->fecha_inicio->format('Y-m-d') }} , Fecha fin:   {{ $modulo->fecha_fin->format('Y-m-d') }}]
                    <br> [ Hora Inicio: {{ $modulo->hora_inicio }}, Hora Salida {{ $modulo->hora_salida }}]
                </small>
                <hr><b>Precio: {{$modulo->costo}} /Bs.</b>
            </li>
        @endforeach
    </ul>


    <h4>Costo Total del los Modulos</h4>
    <ul class="list-group">
        <li class="list-group-item"><b> monto total : {{$reserva->modulos->sum('costo')}}</b></li>
          </ul>

</div>

