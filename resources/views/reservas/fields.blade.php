<!-- Fecha Field -->
<div class="col-lg-12">
    <div class="form-group">
        <label for="form_lastname">Seleciona al Estudiante *</label>
        <select class="form-control select2-simple" name="estudiante_id" style="width: 100%;">
            @foreach($estudiantes  as $dato)
                <option value="{{$dato->id}}"  @if(!(Request::is('*reservas/create')) && ($reserva->estudiante_id==$dato->id))
                selected="selected"
                    @endif
                >
                    {{ $dato->registro }} - {{ $dato->persona->nombre }} {{ $dato->persona->apellido_paterno }} {{ $dato->persona->apellido_materno }}
                </option>
            @endforeach
        </select>
    </div>
</div>


<hr>
{{----------------------------------------------------------------------------------------}}
{{----------------------------------------------------------------------------------------}}
<div class="col-lg-12">
    <div class="form-group">
        <label for="form_lastname">Seleciona los Modulos *</label>
        <select class="form-control select2-multiple"  multiple="multiple" name="modulos_id[]" style="width: 100%;">
            @foreach($categoria  as $datosCategoria)
                @foreach($datosCategoria->programa  as $programa)
                    <optgroup label="  {{$datosCategoria->nombre }} | {{$programa->nombre }}">
                        @foreach($programa->Modulo  as $modulo)
                        <option value="{{$modulo->id}}">

                            {{ $modulo->id }}  - {{ $modulo->nombre }}   [ fecha inicio: {{ $modulo->fecha_inicio->format('Y-m-d') }} , Fecha fin:   {{ $modulo->fecha_fin->format('Y-m-d') }}] [ Hora Inicio: {{ $modulo->hora_inicio }}, Hora Salida {{ $modulo->hora_salida }})

                        </option>
                        @endforeach
                @endforeach
            @endforeach
        </select>
    </div>
</div>
{{----------------------------------------------------------------------------------------}}

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('reservas.index') }}" class="btn btn-default">Cancel</a>
</div>
