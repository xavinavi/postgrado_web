
<div class="form-group col-sm-12">


    <h4>Datos Del Estudiante</h4>
    <ul class="list-group">
        <li class="list-group-item"><b>Codigo Estudiante:</b> {{ $estudiante->registro }} </li>
        <li class="list-group-item"><b>Carnet de Identidad: </b> {{$estudiante->Persona->ci}}  </li>
        <li class="list-group-item"><b>Nombre: </b> {{ $estudiante->Persona->nombre }} {{ $estudiante->Persona->apellido_paterno }} {{ $estudiante->Persona->apellido_materno }}</li>
        <li class="list-group-item"><b>Apellido Paterno: </b> {{ $estudiante->Persona->apellido_paterno }} </li>
        <li class="list-group-item"><b>Apellido Materno: </b>  {{ $estudiante->Persona->apellido_materno }}</li>
        <li class="list-group-item"><b>Telefono: </b>  {{ $estudiante->Persona->telefono }}</li>
        <li class="list-group-item"><b>Direccion: </b> {{ $estudiante->Persona->direccion }}</li>
        <li class="list-group-item"><b>Genero: </b>{{ $estudiante->Persona->genero }}</li>
    </ul>

</div>

