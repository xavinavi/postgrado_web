<div class="table-responsive">
    <table class="table" id="estudiantes-table">
        <thead>
        <tr>
            <th>Ci</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Telefono</th>
            <th>Direccion</th>
            <th>Genero</th>
            <th>Registro</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($estudiantes as $estudiante)
            <tr>
                <td>{{ $estudiante->ci }}</td>
                <td>{{ $estudiante->nombre }}</td>
                <td>{{ $estudiante->apellido_paterno }}</td>
                <td>{{ $estudiante->apellido_materno }}</td>
                <td>{{ $estudiante->telefono }}</td>
                <td>{{ $estudiante->direccion }}</td>
                <td>{{ $estudiante->genero }}</td>
                <td>{{ $estudiante->registro }}</td>
                <td>
                    {!! Form::open(['route' => ['estudiantes.destroy', $estudiante->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('estudiantes.show')

                            <a href="{{ route('estudiantes.show', [$estudiante->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('estudiantes.edit')
                            <a href="{{ route('estudiantes.edit', [$estudiante->id]) }}"
                               class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        @endcan

                        @can('estudiantes.destroy')

                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan

                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
