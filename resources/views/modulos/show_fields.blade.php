<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $modulo->id }}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $modulo->nombre }}</p>
</div>

<!-- Fecha Inicio Field -->
<div class="form-group">
    {!! Form::label('fecha_inicio', 'Fecha Inicio:') !!}
    <p>{{ $modulo->fecha_inicio }}</p>
</div>

<!-- Fecha Fin Field -->
<div class="form-group">
    {!! Form::label('fecha_fin', 'Fecha Fin:') !!}
    <p>{{ $modulo->fecha_fin }}</p>
</div>

<!-- Hora Inicio Field -->
<div class="form-group">
    {!! Form::label('hora_inicio', 'Hora Inicio:') !!}
    <p>{{ $modulo->hora_inicio }}</p>
</div>

<!-- Hora Salida Field -->
<div class="form-group">
    {!! Form::label('hora_salida', 'Hora Salida:') !!}
    <p>{{ $modulo->hora_salida }}</p>
</div>

<!-- Costo Field -->
<div class="form-group">
    {!! Form::label('costo', 'Costo:') !!}
    <p>{{ $modulo->costo }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $modulo->descripcion }}</p>
</div>

<!-- Programa Id Field -->
<div class="form-group">
    {!! Form::label('programa_id', 'Programa Id:') !!}
    <p>{{ $modulo->programa_id }}</p>
</div>

<!-- Profesor Id Field -->
<div class="form-group">
    {!! Form::label('profesor_id', 'Profesor Id:') !!}
    <p>{{ $modulo->profesor_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $modulo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $modulo->updated_at }}</p>
</div>

