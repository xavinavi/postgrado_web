<!-- Nombre Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_inicio', 'Fecha Inicio:') !!}
    {!! Form::date('fecha_inicio',  Request::is('*modulos/create') ? null : $modulo->fecha_inicio, ['class' => 'form-control','id'=>'fecha_inicio']) !!}
</div>



<!-- Fecha Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_fin', 'Fecha Fin:') !!}
    {!! Form::date('fecha_fin',  Request::is('*modulos/create') ? null : $modulo->fecha_fin, ['class' => 'form-control','id'=>'fecha_fin']) !!}
</div>



<!-- Hora Inicio Field -->
<div class="form-group col-sm-3">
    {!! Form::label('hora_inicio', 'Hora Inicio:') !!}
    {!! Form::time('hora_inicio', null, ['class' => 'form-control']) !!}
</div>

<!-- Hora Salida Field -->
<div class="form-group col-sm-3">
    {!! Form::label('hora_salida', 'Hora Salida:') !!}
    {!! Form::time('hora_salida',  Request::is('*modulos/create') ? null : $modulo->hora_salida, ['class' => 'form-control']) !!}
</div>

<!-- Costo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('costo', 'Costo:') !!}
    {!! Form::text('costo', null, ['class' => 'form-control']) !!}
</div>

<!-- Programa Id Field -->

<div class="form-group col-sm-6">
    <div class="form-group">
        <label for="form_lastname">Seleciona el Programa *</label>
        <select class="form-control select2-simple" name="programa_id" style="width: 100%;">
            @foreach($programas  as $data)
                <option value="{{$data->id}}"
                        @if(!(Request::is('*modulos/create')) && ($modulo->programa_id==$data->id))
                        selected="selected"
                    @endif
                >
                    {{$data->id}}  - {{ $data->nombre }}
                </option>
            @endforeach
        </select>
    </div>
</div>


<!-- Profesor Id Field -->



<div class="form-group col-sm-6">
    <div class="form-group">
        <label for="form_lastname">Seleciona el Profesor *</label>
        <select class="form-control select2-simple" name="profesor_id" style="width: 100%;">
            @foreach($profesores  as $data)
                <option value="{{$data->id}}"
                        @if(!(Request::is('*modulos/create')) && ($modulo->profesor_id==$data->id))
                selected="selected"
                    @endif
                >
                    {{$data->id}}  - {{ $data->persona->nombre }}  {{ $data->persona->apellido_paterno }}  {{ $data->persona->apellido_materno }}  ( {{ $data->profesion }} )
                </option>
            @endforeach
        </select>
    </div>
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('modulos.index') }}" class="btn btn-default">Cancel</a>
</div>



@section('scripts')
    <script type="text/javascript">
        $('#fecha_fin').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#fecha_inicio').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>


@endsection
