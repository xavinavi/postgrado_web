<div class="table-responsive">
    <table class="table" id="modulos-table">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Fecha Inicio</th>
            <th>Fecha Fin</th>
            <th>Hora Inicio</th>
            <th>Hora Salida</th>
            <th>Costo</th>
            <th>Descripcion</th>
            <th>Programa Id</th>
            <th>Profesor Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($modulos as $modulo)
            <tr>
                <td>{{ $modulo->nombre }}</td>
                <td>{{ $modulo->fecha_inicio->format('Y-m-d') }}</td>
                <td>{{ $modulo->fecha_fin->format('Y-m-d') }}</td>
                <td>{{ $modulo->hora_inicio }}</td>
                <td>{{ $modulo->hora_salida }}</td>
                <td>{{ $modulo->costo }}</td>
                <td>{{ $modulo->descripcion }}</td>
                <td>{{ $modulo->programa_id }} - {{$modulo->Programa->nombre }}</td>
                <td>{{ $modulo->profesor_id }}
                    -{{ $modulo->Profesor->persona->nombre }} {{ $modulo->Profesor->persona->apellido_paterno }} {{ $modulo->Profesor->persona->apellido_materno }} </td>
                <td>
                    {!! Form::open(['route' => ['modulos.destroy', $modulo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('modulos.show')
                            <a href="{{ route('modulos.show', [$modulo->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('modulos.edit')
                            <a href="{{ route('modulos.edit', [$modulo->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('modulos.destroy')
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
