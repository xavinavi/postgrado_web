
<div class="form-group col-sm-12">

<h4>Datos De la Inscripcion</h4>

<ul class="list-group">
    <li class="list-group-item"><b>Codigo:</b> {{ $inscripcion->id}} </li>
    <li class="list-group-item"><b>Fecha: </b> {{$inscripcion->fecha}}  </li>
</ul>

    <h4>Datos Del Estudiante</h4>
<ul class="list-group">
    <li class="list-group-item"><b>Codigo Estudiante:</b> {{ $inscripcion->Estudiante->registro }} </li>
    <li class="list-group-item"><b>Carnet de Identidad: </b> {{ $inscripcion->Estudiante->Persona->ci}}  </li>
    <li class="list-group-item"><b>Nombre: </b> {{ $inscripcion->Estudiante->Persona->nombre }} {{ $inscripcion->Estudiante->Persona->apellido_paterno }} {{ $inscripcion->Estudiante->Persona->apellido_materno }}</li>
</ul>



    <h4>Modulos</h4>
    <ul class="list-group">
        @foreach($inscripcion->modulos as $modulo)
            <li class="list-group-item">
                <b> {{ $modulo->nombre }}  </b>
                <br>
                <small> [ fecha inicio: {{ $modulo->fecha_inicio->format('Y-m-d') }} , Fecha fin:   {{ $modulo->fecha_fin->format('Y-m-d') }}]
                    <br> [ Hora Inicio: {{ $modulo->hora_inicio }}, Hora Salida {{ $modulo->hora_salida }}]
                </small>
                <hr><b>Precio: {{$modulo->costo}} /Bs.</b>
            </li>
        @endforeach
    </ul>
</div>
