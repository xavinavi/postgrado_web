<div class="table-responsive">
    <table class="table" id="inscripcions-table">
        <thead>
        <tr>
            <th>Codigo</th>
            <th>Fecha</th>
            <th>User</th>
            <th>Estudiante</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($inscripciones as $inscripcion)
            <tr>
                <td>{{ $inscripcion->id }}</td>
                <td>{{ $inscripcion->fecha }}</td>

                <td>{{ $inscripcion->user_id }} {{ $inscripcion->User->email }}
                    ({{ $inscripcion->User->persona->nombre }} {{ $inscripcion->User->persona->apellido_paterno }} {{ $inscripcion->User->persona->apellido_materno }}
                    )
                </td>
                <td> {{ $inscripcion->Estudiante->registro }}
                    ({{ $inscripcion->Estudiante->Persona->nombre }} {{ $inscripcion->Estudiante->persona->apellido_paterno }} {{ $inscripcion->Estudiante->persona->apellido_materno }}
                    )
                </td>


                <td>
                    @can('inscripcions.show')

                    <a href="{{ route('inscripcions.show', [$inscripcion->id]) }}" class='btn btn-default btn-xs'>boleto de inscripcion<i
                            class="glyphicon glyphicon-eye-open"></i></a>
                    @endcan
                   @can('inscripcions.materia')

                        <a href="{{ route('inscripcion.materia', [$inscripcion->id]) }}" class='btn btn-default btn-xs'>calificacion<i class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                    {{----
                    {!! Form::open(['route' => ['inscripcions.destroy', $inscripcion->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('inscripcions.edit', [$inscripcion->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}

                    --}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
