@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            NOTAS DE LOS MODULOS
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <div class="form-group col-sm-12">
                        @include('flash::message')

                        <h4>Datos De la Inscripcion</h4>

                        <ul class="list-group">
                            <li class="list-group-item"><b>Codigo:</b> {{ $inscripcion->id}} </li>
                            <li class="list-group-item"><b>Fecha: </b> {{$inscripcion->fecha}}  </li>
                        </ul>

                        <h4>Datos Del Estudiante</h4>
                        <ul class="list-group">
                            <li class="list-group-item"><b>Codigo
                                    Estudiante:</b> {{ $inscripcion->Estudiante->registro }} </li>
                            <li class="list-group-item"><b>Carnet de
                                    Identidad: </b> {{ $inscripcion->Estudiante->Persona->ci}}  </li>
                            <li class="list-group-item">
                                <b>Nombre: </b> {{ $inscripcion->Estudiante->Persona->nombre }} {{ $inscripcion->Estudiante->Persona->apellido_paterno }} {{ $inscripcion->Estudiante->Persona->apellido_materno }}
                            </li>
                        </ul>
                        <hr>
                        <div class="table-responsive">
                            <table class="table" id="inscripcions-table"  style="width:100%" border=1 id="reservas-table">
                                <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Materia / modulo</th>
                                    <th>calificacion</th>

                                    <th colspan="3">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($inscripcion->modulos as $modulo)
                                    <tr>
                                        <td>{{ $modulo->id }}</td>
                                        <td>{{ $modulo->nombre }}</td>
                                        @if($modulo->pivot->calificaciones)
                                        <td>{{ $modulo->pivot->calificaciones }}</td>
                                        @else
                                            <td>INS</td>

                                        @endif



                                        <td>
                                            @if(!$modulo->pivot->calificaciones)
                                                {!! Form::open(['route' =>['inscripcion.calificar',$modulo->pivot->id]]) !!}
                                                <div class="form-group ">
                                                    {!! Form::number('calificacion', null, ['class' => 'form-control']) !!}
                                                </div>
                                                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

                                                {!! Form::close() !!}

                                            @endif
                                            {{----
                                            {!! Form::open(['route' => ['inscripcions.destroy', $inscripcion->id], 'method' => 'delete']) !!}
                                            <div class='btn-group'>
                                                <a href="{{ route('inscripcions.edit', [$inscripcion->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                            </div>
                                            {!! Form::close() !!}

                                            --}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <a href="{{ route('inscripcions.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection




