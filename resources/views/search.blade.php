



@extends('layouts.app')


<style>
    .skin-blue .sidebar-form input[type="text"], .skin-blue .sidebar-form .btn {
        box-shadow: none;
        background-color: #f4f4f4 !important;
        border: 1px solid transparent;
        height: 35px;
        border: solid 1px #000000;
    }

</style>
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Busqueda</h1>
        <br>
        <form action="{{ route('search') }}" method="POST" class="sidebar-form" >
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="input-group">
                        <input type="text"   name="query" class="form-control" placeholder="Buscar..." >
                        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </span>
                    </div>
                </div>
            </div>
        </form>

    </section>
    <div class="content">
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <div class="box box-primary">

            @if($searchResults)
            <div class="box-body">
                @include('flash::message')
                <h2>
                    {{ $searchResults->count() }} resultados encontrados para "{{ request('query') }}"
                </h2>
                <hr>
                <div class="card-body">

                    @foreach($searchResults->groupByType() as $type => $modelSearchResults)
                        <h3>{{ ucfirst($type) }}</h3>

                        @foreach($modelSearchResults as $searchResult)
                            <ul>
                                <li><a href="{{ $searchResult->url }}">{{ $searchResult->title }}</a></li>
                            </ul>
                        @endforeach
                    @endforeach

                </div>
            </div>
                @else
                <div class="box-body">
                        @include('flash::message')
                    <hr>
                    <div class="card-body">
                    </div>
                </div>

                @endif

        </div>

    </div>
@endsection



