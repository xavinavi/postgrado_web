@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Agenda
        </h1>

    </section>


    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('agendas.show_fields')
                    <a href="{!! route('agendas.index') !!}" class="btn btn-default">Back</a>
                    <a href="{!! route('agendas.edit',$agenda->id) !!}" class="btn btn-primary">Modificar</a>
                    {!! Form::open(['route' => ['agendas.destroy', $agenda->id], 'method' => 'delete']) !!}
                    <br>

                        {!! Form::button('<i class="glyphicon glyphicon-trash"> ELIMINAR</i>', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger ',
                            'onclick' => "return confirm('Are you sure?')",
                            'value'=>'asd'
                        ]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
