<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $agenda->id !!}</p>
</div>

<!-- fecha_inicio Field -->
<div class="form-group">
    {!! Form::label('fecha_inicio', 'fecha_inicio:') !!}
    <p>{!! $agenda->fecha_inicio !!}</p>
</div>

<!-- fecha_fin Field -->
<div class="form-group">
    {!! Form::label('fecha_fin', 'fecha_fin:') !!}
    <p>{!! $agenda->fecha_fin !!}</p>
</div>

<!-- Titutlo Field -->
<div class="form-group">
    {!! Form::label('titutlo', 'Titutlo:') !!}
    <p>{!! $agenda->titutlo !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $agenda->descripcion !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'Evento Creado por') !!}
    <p> ({!! $agenda->user->id !!} ) {!! $agenda->user->name !!}  {!! $agenda->user->email !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $agenda->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $agenda->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $agenda->deleted_at !!}</p>
</div>



