<!-- fecha_inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_inicio', 'fecha_inicio:') !!}
    {!! Form::date('fecha_inicio', null, ['class' => 'form-control','id'=>'fecha_inicio']) !!}
</div>



<!-- fecha_fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_fin', 'fecha_fin:') !!}
    {!! Form::date('fecha_fin', null, ['class' => 'form-control','id'=>'fecha_fin']) !!}
</div>


<!-- Titutlo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('titutlo', 'Titutlo:') !!}
    {!! Form::text('titutlo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textArea('descripcion', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('agendas.index') !!}" class="btn btn-default">Cancel</a>
</div>
