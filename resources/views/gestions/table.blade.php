<div class="table-responsive">
    <table class="table" id="gestions-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Año</th>
            <th>Descripcion</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($gestions as $gestion)
            <tr>
                <td>{{ $gestion->id }}</td>
                <td>{{ $gestion->anio }}</td>
                <td>{{ $gestion->descripcion }}</td>
                <td>
                    {!! Form::open(['route' => ['gestions.destroy', $gestion->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        @can('gestions.show')
                            <a href="{{ route('gestions.show', [$gestion->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        @endcan
                        @can('gestions.edit')
                            <a href="{{ route('gestions.edit', [$gestion->id]) }}" class='btn btn-default btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        @endcan
                        @can('gestions.destroy')
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endcan
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
