@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Gestion
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                 {!! Form::model($gestion, ['route' => ['gestions.update', $gestion->id], 'method' => 'patch']) !!}

                        @include('gestions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
