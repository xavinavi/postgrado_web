<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $gestion->id }}</p>
</div>

<!-- Año Field -->
<div class="form-group">
    {!! Form::label('año', 'Año:') !!}
    <p>{{ $gestion->anio }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $gestion->descripcion }}</p>
</div>

